exports = exports ? this
exports.views = exports.views ? {}
exports.views.common = exports.views.common ? {}


ReasoningWidget = Backbone.View.extend(
    initialize: () ->
        this.render()

    render: () ->
        template = _.template( $("#template_reasoning_details").html() )
        this.$el.html( template() )        
)


exports.views.common.ReasoningWidget = ReasoningWidget
