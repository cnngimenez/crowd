<?php 
/* 

   Copyright 2016 Giménez, Christian
   
   Author: Giménez, Christian   

   importjson.php
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


?>
<div class="modal fade importjson-popup" tabindex="-1" role="dialog"
     id="importjson_widget"
     aria-labelledby="importjson_widget" aria-hidden="true">
    <div class="modal-dialog" role="document">
	<div class="modal-content">
	   
	    <div class="modal-header">
		<h1 class="modal-title">Import JSON</h1>
		<button type="button" class="close" data-dismiss="modal"
			aria-label="close">
		    <span aria-hidden="true">&times;</span>
		</button>
	    </div>
	    
	    <div class="modal-body">
		<p>Paste the JSON string here</p>
		<textarea class="form-control" id="importjson_input"></textarea>
	    </div>
	    
	    <div class="modal-footer">
		<div class="btn-group" role="group">
		    <button type="button" id="importjson_importbtn"
		       class="btn btn-primary">
			Import
		    </button>
		    <button type="button" data-dismiss="modal"
		       class="btn btn-danger">
			Cancel
		    </button>
		</div>
	    </div>

	</div>
    </div>
</div>
