<?php 
/* 

   Copyright 2017 Giménez, Christian
   
   Author: Giménez, Christian   

   save_load_json.php
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

?>
<div id="saveloadjsonwidget" class="saveloadjsonwidget modal" tabindex="-1" role="dialog">

    <div class="modal-dialog" role="document">
	<div class="modal-content">
	    <div class="modal-header">
		<h1 class="modal-title"> Load / Save</h1>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		    <span aria-hidden="true">&times;</span>
		</button>
	    </div>

	    <div class="modal-body">
		<div class="row">
		    <div class="col">
			
			<div class="card">
			    <div class="card-header">
				<h1 id="savejson-header" class="modal-title">Save</h1>
				
			    </div><!-- card-header -->
			    
			    <div class="card-body">
				<form id="savejson-form">
				    <label>Model name</label>
				    <input type="text" class="form-control" id="savejson_name" />
				</form>			
			    </div><!-- card-body -->

			    <div class="card-footer">
				<button class="btn btn-primary"
					type="button" id="savejson_save_btn">
				    Save
				</button>
			    </div>
			</div><!-- card -->

		    </div>
		    <div class="col">
			
			<div class="card">
			    <div class="card-header">
				<h1 id="loadjson-header">Load</h1>			
			    </div><!-- card-header -->
			    
			    <div class="card-body">
				<form id="loadjson-form">
				    <ul data-role="listview" id="modelList" data-inset="true"
						   data-autodividers="true" data-filter="true"
						   data-filter-placeholder="Model...">
				    </ul>
				</form>		
			    </div><!-- card-body -->		    
			</div><!-- card -->
		    </div>
		</div><!-- row -->
	    </div>
	</div>
    </div>
 </div>
