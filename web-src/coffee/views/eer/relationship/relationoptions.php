<?php
/*

   Copyright 2016 Giménez, Christian

   Author: Giménez, Christian

   relationoptions.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


?>


<div class="relationOptions" style="visible:false, z-index:1, position:absolute">
    <input type="hidden" id="relationoptions_classid"
	   name="classid"  value="<%= classid %>" />
    <div style="float: left">
	<form id="left-rel">
    	    <input type="hidden" id="relationoptions_classid"
		   name="classid" value="<%= classid %>" />
    	    <input class="form-control" placeholder="0" type="text"
		   id="cardfrom-1"
		   size="2" maxlength="4" />
    	    <input type="hidden" id="relationoptions_classid"
		   name="classid" value="<%= classid %>" />
    	    <input class="form-control" placeholder="*" type="text"
		   id="cardfrom-2" size="2" maxlength="4" />
	    <input class="form-control"
		   placeholder="role1" type="text"
		   id="role-from" size="2" maxlength="4" />
	    </form>
    </div>

    <div style="float: left">
	<form id="name-rel">
	    <input placeholder="name" type="text"
		   size="4" maxlength="10" id="assoc_name" />
	        <div class="btn-group" role="group">
		    <button class="btn btn-primary btn-sm" type="button"
			    id="eerassociation_button">
			Association
		    </button>
		    <button class="btn btn-secondary btn-sm" type="button"
			    id="eerassoc_class_button">
			Association Class
		    </button>
	        </div>
	</form>
    </div>

    <input type="hidden" id="relationoptions_classid"
	   name="classid"  value="<%= classid %>" />
    <div style="float: right">
	<form id="right-rel">
    	    <input type="hidden" id="relationoptions_classid"
		   name="classid" value="<%= classid %>" />
    	    <input class="form-control"
		   placeholder="0" type="text"
		   id="cardto-1" size="2" maxlength="4" />
    	    <input type="hidden" id="relationoptions_classid"
		   name="classid" value="<%= classid %>" />
    	    <input class="form-control" 
		   placeholder="*" type="text"
		   id="cardto-2" size="2" maxlength="4" />
	    <input class="form-control"
		   placeholder="role2" type="text"
		   id="role-to" size="2" maxlength="4" />
	</form>
    </div>
</div>
