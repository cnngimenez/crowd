<?php
/*

   Copyright 2017 Giménez, Christian

   Author: Giménez, Christian

   tools_uml.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


?>
<div class="navbar-nav">    
    <a class="nav-item nav-link" id="eerentity_button" href="#">Entity</a>
    <a class="nav-item nav-link disabled" id="eerassoc_button" href="#">Assoc</a>
    <a class="nav-item nav-link disabled" id="eerisa_button" href="#">IS-A</a>
    <a class="nav-item nav-link" id="eerattr_button" href="#">Attr</a>
</div>
