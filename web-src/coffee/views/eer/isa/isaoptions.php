<?php
/*

Copyright 2017, Grupo de Investigación en Lenguajes e Inteligencia Artificial (GILIA)

Author: Facultad de Informática, Universidad Nacional del Comahue

isaoptions.php

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


?>

<div class="modal fade" id="eer_isaoptions" tabindex="-1"
role="dialog" aria-labelledby="isaoptions" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-header">Is-A Constraints</h1>
        <button type="button" class="close" data-dismiss="modal"
			      aria-label="close">
		          <span aria-hidden="true">&times;</span>
		    </button>
      </div>

      <div class="modal-body">
        <input type="hidden" id="relationoptions_classid"
	       name="classid"  value="<%= classid %>" />
         <button class="btn btn-secondary btn-sm" type="button"
          id="eerisa_button">
	         Is A
         </button>
    <fieldset data-role="controlgroup" data-type="horizontal"
    data-mini="true">
	<input type="checkbox" name="chk-covering" id="chk-covering"/>
	<label for="chk-covering">Covering</label>
	<input type="checkbox" name="chk-disjoint" id="chk-disjoint" />
	<label for="chk-disjoint">Disjoint</label>
    </fieldset>
    </div>
  </div>
</div>
</div>
