# controller.coffee --
# Copyright (C) 2018 GILIA

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.views = exports.views ? {}
exports.views.uml = exports.views.uml ? {}

# UMLController
#
# @namespace views.uml
class UMLController

    constructor: () ->

    add_class: () ->

    add_assoc: () ->
        gui.uml.iumlwidgets.relationoptions.show()
    add_gen: () ->

exports.views.uml.UMLController = UMLController

# AddGen
#
# @namespace views.uml
class AddGen extends UMLController

  constructor: () ->
    super()
    @reset()

  reset: () ->
    @children = []
    @parent = null
    @constraints =
        disjoint: null
        covering: null

  run: () ->
    @reset()
    gui.uml.iumlwidgets.toolbar.set_feedback "Constraints"
    gui.uml.iumlwidgets.isaoptions.show @constraint_selected

  # This is the second step. Constraint has been selected and we need the
  # parent.
  constraint_selected: () ->
    @constraints = gui.uml.iumlwidgets.isaoptions.constraints
    gui.uml.iumlwidgets.toolbar.set_feedback "Select Parent"
    gui.gui_instance.set_selection_state
        cell_clicked: (cellid) ->
            views.uml.iaddgen.parent_selected cellid
            # Using @parent_selected won't work because loses the this context
            # Calling using the object we ensure that the iaddgen object is the
            # one receiving the parent_selected() call.
            # Don't use cell_clicked: views.uml.iaddgen.parent_selected.
    
  # A parent has been selected.
  # @param cellid {string}
  parent_selected: (cellid) ->
    @parent = cellid    
    gui.uml.iumlwidgets.toolbar.set_feedback "Select children"
    gui.uml.iumlwidgets.toolbar.show_done () ->
        views.uml.iaddgen.done_selected()

    gui.uml.iguiuml.set_selection_state
        cell_clicked: (cellid) ->
            views.uml.iaddgen.child_selected cellid

  # A child has been selected.
  # @param cellid {string}
  child_selected: (cellid) ->
    if (cellid in @children)
        @children = @children.filter (elt) ->
            elt is not cellid
    else
        @children.push cellid

  # The done button has been clicked.
  #
  # Create the Is-A relationship.
  done_selected: () ->
    gui.uml.iumlwidgets.toolbar.set_feedback ""
    gui.uml.iumlwidgets.toolbar.hide_done()

    console.log @parent
    console.log @children
    console.log @constraints
    gui.uml.iumladapter.diag.add_generalization @parent, @children,
      @constraints.disjoint, @constraints.covering

exports.views.uml.AddGen = AddGen

# Instance of {UMLController} for easy accessing.
#
# @namespace views.uml
exports.views.uml.iumlcontroller = new UMLController()

# @namespace views.uml
exports.views.uml.iaddgen = new AddGen()
