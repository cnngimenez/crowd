<?php
/*

   Copyright 2016 Giménez, Christian

   Author: Giménez, Christian

   relationoptions.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


?>


<div class="relationOptions" style="visible:false, z-index:1, position:absolute">
    
    <form>
	<input type="hidden" id="umlrelationoptions_classid"
	       name="classid"  value="<%= classid %>" />

	<input placeholder="name" type="text" id="umlassoc_name"
	       class="form-control form-control-sm" />
	
	<div class="input-group">
    	    <input placeholder="0" type="text" id="umlcardfrom-1" 
		   class="form-control form-control-sm" />
    	    <input placeholder="*" type="text" id="umlcardfrom-2" 
		   class="form-control form-control-sm" />
	</div>
	<input placeholder="role1" type="text" id="umlrole-from"
	       class="form-control form-control-sm" />

	<div class="input-group">
	    <input placeholder="0" type="text" id="umlcardto-1"
		   class="form-control form-control-sm" />	    
	    <input placeholder="*" type="text" id="umlcardto-2"
		   class="form-control form-control-sm" />
	</div>
	
	<input placeholder="role2" type="text" id="umlrole-to"
	       class="form-control form-control-sm" />
    </form>

    <div class="btn-group btn-group-sm" role="group">
	<button class="btn btn-sm btn-primary" type="button"
		id="umlassociation_button">
	    Association
	</button>
	<button class="btn btn-sm btn-secondary" type="button"
		id="umlassoc_class_button">
	    Association Class
	</button>
	<button type="button" class="btn btn-danger"
		id="umlassoc_cancelbtn">
	    Cancel
	</button>
    </div>
    
</div>
