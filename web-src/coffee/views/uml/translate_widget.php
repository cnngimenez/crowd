<?php 
/* 

   Copyright 2018 Giménez, Christian
   
   Author: Giménez, Christian   

   translate_widget.php
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


?>

<div class="modal fade" id="uml_translate_widget" tabindex="-1" role="dialog"
     aria-labelledby="uml_translate_widget" aria-hidden="true">

    <div class="modal-dialog" role="document">
	<div class="modal-content">
            <div class="modal-header">
		<h3 class="modal-title">UML Formalization</h3>
		<button type="button" class="close" data-dismiss="modal"
			aria-label="close">
		    <span aria-hidden="true">&times;</span>
		</button>
	    </div>

	    <div class="modal-body">
		Translation method:
		<select class="custom-select" id="uml-translation-method">
		    <option selected="1" value="berardi">Berardi</option>
		    <option value="crowd">crowd's Metamodelling</option>
		</select>

		Output syntax:
		<select class="custom-select" id="uml-output-type">
		    <option selected="1" value="owl">OWL 2</option>
		    <option value="html">DL in HTML</option>
		</select>

		Formalization Results:
		<div id="html-output"></div>
		<textarea class="form-control" cols="10"
			  id="uml-owllink-source"></textarea>		
            </div>

            <div class="modal-footer">
		<div class="btn-group" role="group">
		    <button type="button" class="btn btn-primary"
			    id="uml-translate-btn">
			Translate
		    </button>
		    <button type="button" class="btn btn-secondary"
			    data-dismiss="modal">
			Hide
		    </button>
		</div>
            </div>
	    
        </div>	
    </div>
    
</div>
