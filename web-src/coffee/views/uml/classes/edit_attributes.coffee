# edit_attributes.coffee --
# Copyright (C) 2017 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.



exports = exports ? this
exports.views = exports.views ? this
exports.views.uml = exports.views.uml ? this
exports.views.uml.classes = exports.views.uml.classes ? this

# @mixin
# @namespace views.uml.classes
# @extend Backbone.View
# 
# Widget for editing UML class attributes.
#
# Provides options for creating, removing and listing attributes.
# 
EditAttributes = Backbone.View.extend(
    initialize: () ->
        this.render()
        @hide()

    render: () ->
        attrs = ''
        if @umlclass?
            attrs = String(@umlclass.attrs)

        template = _.template( $("#template_edit_attributes").html() )

        this.$el.html(template( lst_attrs: attrs ))

    events:
        "click button#umladd_attr_button": "add_attr"
        "click button#umlback_attr_button": "back"

    set_classid: (@classid) ->
        @umlclass = gui.gui_instance.current_gui.diag.find_class_by_classid(@classid)
        viewpos = graph.getCell(@classid).findView(paper).getBBox()

        @listenTo(@umlclass, 'change', @render)

        this.$el.css(
            top: viewpos.y + 50,
            left: viewpos.x,
            position: 'absolute',
            'z-index': 1
            )

    # Display the dialog over the UML class.
    #
    # Need the @classid setted. See set_classid().
    show: () ->
        @render()
        this.$el.show()

    hide: () ->
        this.$el.hide()

    # Add a new attribute from the text in the input field.
    add_attr: () ->
        attrstr = $("input#umlattr_input").val()
        @umlclass.add_attr(attrstr)
        @render()

    # Hide this dialog.
    back: () ->
        @hide()
        
)

exports.views.uml.classes.EditAttributes = EditAttributes
