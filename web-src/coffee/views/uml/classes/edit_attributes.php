<?php 
/* 

   Copyright 2017 Giménez, Christian
   
   Author: Giménez, Christian   

   edit_attributes.php
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


?>
<form>
    <div class="container" id="umlattr_list">
	<%= lst_attrs %>
    </div>
    
    <input class="form-control" placeholder="Attribute description"
	   type="text" id="umlattr_input" />

    <div class="btn-group" role="group">    
	<button class="btn btn-sm btn-primary" type="button"
		id="umladd_attr_button">
	    Add
	</button>
	<button class="btn btn-sm btn-secondary"  type="button"
		id="umlback_attr_button">
	    Close
	</button>
    </div>
</form>
