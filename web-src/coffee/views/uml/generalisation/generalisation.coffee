# generalisation.coffee --
# Copyright (C) 2016 GILIA

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.views = exports.views ? this
exports.views.uml = exports.views.uml ? this
exports.views.uml.generalisation = exports.views.uml.generalisation ? this

# Generalization Options Modal Window
#
# # Relevant Attributes
# 
# * @classid: The class id setted as a parent class. Not used and can be ignored.
# * @constraints: Constraints setted for the new Is-A relationship. Setted when
#   new_isa() is called or "Is-A" button is clicked.
GeneralisationOptionsView = Backbone.View.extend
    initialize: () ->
        this.render()
        @classid = null
        @constraints =
            disjoint: null
            covering: null
        # @hide()

    render: () ->
        template = _.template( $("#template_generalisationoptions").html() )
        this.$el.html(template
            classid: @classid
            )

    events:
        'click button#umlisa_button' : 'new_isa'
        'click button#umlisa_cancelbtn' : 'hide'

    new_isa: () ->
        @constraints.disjoint = $("#chk-disjoint").prop "checked"
        @constraints.covering = $("#chk-covering").prop "checked"
        @hide()
        if @callback?
            @callback this

    set_classid: (@classid) ->
        @show()

    get_classid: () ->
        return @classid

    hide: () ->
        this.$el.children(0).modal "hide"

    show: (callback = null) ->
        if callback?
            @callback = callback
        this.$el.children(0).modal "show"




exports.views.uml.generalisation.GeneralisationOptionsView = GeneralisationOptionsView
