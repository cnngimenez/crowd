<?php
/*

   Copyright 2017, Grupo de Investigación en Lenguajes e Inteligencia Artificial (GILIA)

   Author: Facultad de Informática, Universidad Nacional del Comahue

   isaoptions.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


?>


<div class="isaOptions modal fade" id="uml_isaOptions" tabindex="-1" role="dialog"
     aria-labelledby="error_widget" aria-hidden="true">
    <div class="modal-dialog" role="document">
	<div class="modal-content">
	    
	    <div class="modal-header">
		<h3 class="modal-title">Is-A</h3>
		<button type="button" class="close" data-dismiss="modal"
			      aria-label="close">
		    <span aria-hidden="true">&times;</span>
		</button>
	    </div>

	    <div class="modal-body">	    
		<div class="custom-control custom-checkbox">
		    <input class="custom-control-input"
			   type="checkbox"
			   name="chk-covering" id="chk-covering"/>
		    <label class="custom-control-label"
			   for="chk-covering">
			Covering
		    </label>
		</div>
		<div class="custom-control custom-checkbox">
		    <input type="checkbox" class="custom-control-input"
			   name="chk-disjoint" id="chk-disjoint" />
		    <label class="custom-control-label"
			   for="chk-disjoint">
			Disjoint
		    </label>
		</div>
	    </div>

	    <div class="modal-footer">
		<div class="btn-group" role="group">
		    <button class="btn btn-sm btn-primary"
			    type="button" id="umlisa_button">
			Is A
		    </button>
		    <button type="button" class="btn btn-sm btn-danger"
			    id="umlisa_cancelbtn">
			Cancel
		    </button>
		</div>
	    </div>
	    
	</div>
    </div>
</div>
