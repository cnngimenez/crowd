# reasoning_widget.coffee --
# Copyright (C) 2018 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


exports = exports ? this
exports.views = exports.views ? {}
exports.views.uml = exports.views.uml ? {}


ReasoningWidget = Backbone.View.extend
    initialize: () ->
        this.render()

    render: () ->
        template = _.template $("#template_reasoning_widget").html()
        this.$el.html template({})

    events:
        'click button#uml-run-btn': 'run_reasoner'

    get_strategy: () ->
        this.$el.find("select#uml-reasoning-strat").val()

    get_reasoner: () ->
        this.$el.find("select#uml-reasoning-program").val()
        
    run_reasoner: () ->
        strat = this.get_strategy()
        reasoner = this.get_reasoner()
        gui.gui_instance.full_reasoning strat, reasoner

    show: () ->
        this.$el.find('#uml_reasoning_widget').modal('show')

    hide: () ->
        this.$el.find('#uml_reasoning_widget').modal('hide')


exports.views.uml.ReasoningWidget = ReasoningWidget
