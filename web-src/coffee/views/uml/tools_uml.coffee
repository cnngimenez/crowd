# tools_uml.coffee --
# Copyright (C) 2017 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


exports = exports ? this
exports.views = exports.views ? {}
exports.views.uml = exports.views.uml ? this


# UML Toolbar.
#
# Acoplable toolbar for the most common primitives creation.
#
# # Callbacks
#
# The `callbacks` object has the following configurable callbacks:
#
# * done : see show_done(). What to do when the done is clicked.
# 
ToolsUMLView = Backbone.View.extend(
    initialize: () ->
        @render()
        @classnum = 0
        @callbacks =
            done: null

    render: () ->
        template = _.template( $("#template_tools_uml").html(), {} )
        this.$el.html template
        @hide_done()

    events:
        'click a#menu-check-consistency': 'check_consistency'
        'click a#menu-translate-uml': 'translate'
        'click a#umlclass_button': 'umlclass_pressed'
        'click a#umlassoc_button': 'umlassoc_pressed'
        'click a#umlgen_button': 'umlgen_pressed'
        'click button#uml_donebtn': 'umldone_pressed'

    # What to do when the done button is pressed.
    # 
    # Hide the done button and call the callback.
    umldone_pressed: () ->
        @hide_done()
        @callbacks.done()

    umlclass_pressed: () ->
        @classnum += 1
        gui.gui_instance.current_gui.diagadapter.add_object_type
            name: "Class" + @classnum

    umlassoc_pressed: () ->
        console.log 'umlassoc'

    umlgen_pressed: () ->
        console.log 'umlisa'
        views.uml.iaddgen.run()

    translate: () ->
        gui.gui_instance.current_gui.widgets.translatewidget.show()

    check_consistency: () ->
        gui.uml.iumlwidgets.show_reasoning()

    # Write a status message or a short feedback for the user.
    set_feedback: (str) ->
      $("#uml_feedback").html str

    # Show and enable the widget.
    enable: () ->
        this.$el.show()

    # Hide and disable the widget.
    disable: () ->
        this.$el.hide()

    # Display the Done Button and use the given callback when pressed.
    #
    # @param callback [function] A function that is called when the button
    #   is pressed.
    show_done: (callback) ->
        this.$el.find('#uml_donebtn').show()
        @callbacks.done = callback

    # Hide the done button.
    hide_done: () ->
        this.$el.find('#uml_donebtn').hide()
)

exports.views.uml.ToolsUML = ToolsUMLView
