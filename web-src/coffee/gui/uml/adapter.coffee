# adapter.coffee --
# Copyright (C) 2018 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


exports = exports ? this
exports.gui = exports.gui ? {}
exports.gui.uml = exports.gui.uml ? {}


# @namespace gui.uml
#
# DiagAdapter for UML diagrams.
class UMLAdapter extends gui.DiagAdapter
    constructor: (diag, paper) ->
        super(diag, paper)
    #
    # Add a class to the diagram.
    #
    # @param hash_data {Hash} data information for creating the Class. Use `name`, `attribs` and `methods` keys.
    # @see Class
    # @see Diagram#add_class
    add_object_type: (hash_data) ->
        @diag.add_class(hash_data)

    #
    # Add a simple association from A to B.
    # Then, set the selection state for restoring the interface.
    #
    # @example Getting a classid
    #   < graph.getCells()[0].id
    #   > "5777cd89-45b6-407e-9994-5d681c0717c1"
    #
    # @param class_a_id {string}
    # @param class_b_id {string}
    # @param name {string} optional. The association name.
    # @param mult {array} optional. An array of two string with the cardinality from class and to class b.
    add_relationship: (class_a_id, class_b_id, name=null, mult=null, roles=null) ->
        @diag.add_association(class_a_id, class_b_id, name, mult, roles)
        gui.gui_instance.current_gui.set_selection_state()

    # Add a Generalization link and then set the selection state.
    #
    # @param class_parent_id {string} The parent class Id.
    # @param class_child_id {string} The child class Id.
    #
    # @todo Support various children on parameter class_child_id.
    add_subsumption: (class_parent_id, class_child_id, disjoint=false, covering=false) ->
        @diag.add_generalization(class_parent_id, class_child_id, disjoint, covering)


# Unique instance of UMLAdapter.
# 
# @namespace gui.uml
exports.gui.uml.iumladapter = null

exports.gui.uml.UMLAdapter = UMLAdapter
