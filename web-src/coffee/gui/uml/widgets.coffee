# widgets.coffee --
# Copyright (C) 2018 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.gui = exports.gui ? {}
exports.gui.uml = exports.gui.uml ? {}

# @namespace gui.uml
#
# Widgets used in UML GUI only.
class UMLWidgets extends gui.WidgetMgr
    constructor: () ->
        super()
        @crearclase = new views.uml.classes.CreateClassView
            el: $("#crearclase_placeholder")
        @toolbar = new views.uml.ToolsUML
            el: $("#lang_tools_placeholder")
        @editclass = new views.uml.classes.EditClassView
            el: $("#editclass_placeholder")
        @editattrs = new views.uml.classes.EditAttributes
            el: $("#uml_editattr_placer")
        @classoptions = new views.uml.classes.ClassOptionsView
            el: $("#classoptions_placeholder")
        @relationoptions = new views.uml.association.AssociationOptionsView
            el: $("#relationoptions_placeholder")
        @isaoptions = new views.uml.generalisation.GeneralisationOptionsView
            el: $("#uml_isaoptions_placeholder")
        @translatewidget = new views.uml.TranslateWidget
            el: $("#uml_translate_placeholder")
        @reasoningwidget = new views.uml.ReasoningWidget
            el: $("#uml_reasoning_placeholder")

    disable: () ->
        # @crearclase.disable()
        @toolbar.disable()
        # @editclass.disable()
        # @editattrs.disable()
        # @classoptions.disable()
        # @relationoptions.disable()
        # @isaoptions.disable()

    enable: () ->
        # @crearclase.enable()
        @toolbar.enable()
        # @editclass.enable()
        # @editattrs.enable()
        # @classoptions.enable()
        # @relationoptions.enable()
        # @isaoptions.enable()

    # Set the class Id of the class options GUI.
    set_options_classid: (model_id) ->
        # @relationoptions.set_classid(model_id)
        @classoptions.set_classid(model_id)
        # @isaoptions.set_classid(model_id)

    # Hide the class options GUI.
    hide_options: () ->
        @classoptions.hide()
        @relationoptions.hide()
        @editclass.hide()
        @isaoptions.hide()

    set_editclass_classid: (model_id) ->
        # editclass = new EditClassView({el: $("#editclass")})
        @editclass.set_classid(model_id)
        @editclass.show()

    # Tell the editattr widget which class it should edit.
    #
    # @param model_id [string] The model (class primitive) to edit.
    set_editattribute_classid: (model_id) ->
        @editattrs.set_classid(model_id)
        @editattrs.show()

    # Tell the @relationoptions widget which class it should edit.
    #
    # @param model_id [string] The model (class primitive) to edit.
    set_editgeneralization_classid: (model_id) ->
        @isaoptions.set_classid(model_id)
        @isaoptions.show()

    # Tell the @isaoptions widget which class it should edit.
    #
    # @param model_id [string] The model (class primitive) to edit.
    set_editassociation_classid: (model_id) ->
        @relationoptions.set_classid(model_id)
        @relationoptions.show()

    show_reasoning: () ->
        @reasoningwidget.show()
    hide_reasoning: () ->
        @reasoningwidget.hide()

# Unique instance of UMLWidgets
#
# @namespace gui.uml
exports.gui.uml.iumlwidgets = new UMLWidgets()

exports.gui.uml.UMLWidgets = UMLWidgets
