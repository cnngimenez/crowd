# gui.coffee --
# Copyright (C) 2016 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.gui = exports.gui ? {}


# @namespace gui
#
# Main GUI class.
#
# - Provides {State} support. Store the @current_state.
# - Contains the paper and graph.
# - Contains {WMCommon} (common widgets) used in all UI.
# - Manage the current GUI (UML, EER, ORM GUI).
#
# Basically manages the common elements of all the GUI interfaces. Every
# behaviour that is common for UML, EER and ORM should be coded here.
class GUI

    # @param graph {joint.Graph}  The JointJS Graph instance.
    # @param paper {joint.Paper}  The JointJS Paper instance.
    constructor: (@graph, @paper) ->
        # Current and active GUIIMP.
        @current_gui = null
        # When changing GUI, this will be the "return" GUI.
        @prev_gui = null
        # List of GUIIMP instance available.
        @lst_guis = {}
        @aux_gui = []

        @widgets = new gui.WMCommon()

        @state = new gui.State()
        @current_state = null
        
        @set_paper_events()

    # # GUIIMP management.
    # Messages for manage GUIIMP instances.
    # ---
    #
    # Add the GUIIMP instance as availables GUIs, also make it the current.
    #
    # @param name {string} The GUIIMP name.
    # @param guiimp {GUIIMP} An instance of a GUIIMP subclass.
    add_gui: (name, guiimp) ->
        @lst_guis[name] = guiimp
        guiimp.set_graph(@graph)
        guiimp.set_paper(@paper)
        this.switch_to_gui(name)

    to_metamodel: () ->

    to_fol: () ->
      @current_gui.to_fol(this)

    # Make the nth GUIIMP in the @lst_guis the current GUI.
    #
    # Change the current GUI the previous one.
    #
    # @param name {string} The GUI name.
    switch_to_gui: (name) ->
        if @lst_guis[name]?
            @current_gui.disable() if @current_gui?
            @prev_gui = @current_gui
            @current_gui = @lst_guis[name]
            @current_gui.enable()

    # Make the GUIIMP instance that it is in the @lst_guis as the previous GUI.
    #
    # @param name {name} The name key of the @lst_guis.
    set_previous: (name) ->
        if @lst_guis[name]?
            @prev_gui = @lst_guis[name]

    # Set the previous GUIIMP instance that may not be added to my @lst_guis.
    #
    # @param guiimp {GUIIMP} A GUIIMP subclass instance.
    set_prev_gui: (guiimp) ->
        # @prev_gui = new gui.GUIEER(@graph,@paper)
        @prev_gui = guiimp
        guiimp.graph = @graph
        guiimp.paper = @paper

    # Switch to the previous GUIIMP.
    switch_to_prev: () ->
        @aux_gui = @current_gui
        @current_gui = @prev_gui
        @prev_gui = @aux_gui

    update_metamodel: (data) ->
        @current_gui.update_metamodel(data)

    update_fol: (data) ->
        @current_gui.update_fol(data)
    # Translate the current model into a formalization.
    #
    # Show the user a "wait" message while the server process the model.
    #
    # @param strategy {String} The strategy name to use for formalize the model.
    # @param syntax {String} The output syntax format.
    translate_formal: (strategy, syntax) ->
        @current_gui.translate_formal(strategy, syntax)


    # Full reasoning on the current model and selecting a reasoner.
    #
    # Show the user a "wait" message while the server process the model.
    #
    # @param strategy {String} model encoding required for reasoning on.
    # @param syntax {String} reasoning system.
    full_reasoning: (strategy, reasoner) ->
        @current_gui.full_reasoning(strategy, reasoner)


    # Update traffic light and models after full reasoning
    #
    # Draft version: only update textarea with data
    #
    # @param data {string} OWLlink string
    update_full_reasoning: (data) ->
        @current_gui.update_full_reasoning(data)


    # @deprecated Use translate_formal() instead.
    translate_owllink: () ->
        @current_gui.translate_owllink(this)

    update_translation: (data) -> @current_gui.update_translation(data)

    set_isa_state: (class_id, disjoint, covering) ->
        @current_state = @state.isa_state()
        @current_gui.set_isa_state @current_state, class_id, disjoint, covering

    set_association_state: (class_id, mult, roles, name) ->
        @current_state = @state.association_state()
        @current_gui.set_association_state(class_id, mult, roles, name)

    # Change the GUI State into "selection state", where the can select a
    # Joint cell.
    #
    # @param callback {object} Callbacks functions. See {State} class.
    set_selection_state: (callbacks = null) ->
        @current_state = @state.selection_state()
        @current_state.callbacks = callbacks
        @current_gui.set_selection_state callbacks

    clear_relationship: () ->
      @current_gui.clear_relationship()


    # Take control of the paper events.
    #
    # Set the cell:pointerclick events and others.
    set_paper_events: () ->
        @paper.on "cell:pointerclick", (cellview, evt, x, y) =>
            @on_cell_clicked cellview, evt, x, y
        
    # What to do when a Joint cell is clicked.
    #
    # Follows the responsability to the @current_state.
    #
    # @param cellview [dia.CellView] The Joint CellView that recieves
    #   the click event.
    # @param event [Event] The event descripiton object.
    # @param x [number] X coordinate.
    # @param y [number] Y coordinate.
    on_cell_clicked: (cellview, event, x, y) ->
        if @current_state?
            @current_state.on_cell_clicked cellview, event, x, y, this
        else
            @current_gui.on_cell_clicked cellview,event,x,y
        

    # Check if the model is satisfiable sending a POST to the server.
    check_satisfiable: () ->
        @current_gui.check_satisfiable()

    # Update the diagram displaying unsatisfiable primitives in red.
    #
    # @param data [String] The JSON answer.
    update_satisfiable: (data) ->
        @current_gui.update_satisfiable(data)
     
# Current GUI instance.
#
# An instance must be running!
#
# Better use gui.set_current_instance()
#
# @see set_current_instance()
# @namespace gui
exports.gui.gui_instance = null

# Set the current instance of the GUI class.
#
# This has nothing to do with the current language interface (GUIUML, GUIEER,
# etc.).
#
# @param gui_instance {GUI} The running instance.
# @namespace gui
exports.gui.set_current_instance = (gui_instance) ->
    gui.gui_instance = gui_instance


# @namespace gui
#
# Switch to the ERD interface and diagram.
#
# This is sooo bad, but the context of a $.post callback function
# differs from the source caller class.
#
# We need to set a global gui instance variable with one GUI.gui instance.
exports.gui.switch_to_erd = () ->
    gui.gui_instance.aux_gui = gui_instance.current_gui
    gui.gui_instance.current_gui = gui_instance.prev_gui
    gui.gui_instance.prev_gui = gui_instance.aux_gui

# @namespace gui
#
# This is sooo bad, but the context of a $.post callback function
# differs from the source caller class.
#
# We need to set a global guiinst variable with one GUI.gui instance.
exports.gui.update_satisfiable = (data) ->
    exports.gui.gui_instance.update_satisfiable(data)

# @namespace gui
exports.gui.update_translation = (data) ->
    exports.gui.gui_instance.update_translation(data)

# @namespace gui
#
# Event for 
exports.gui.show_error = (jqXHR, status, text) ->
    exports.gui.gui_instance.widgets.show_error(
        status + ": " + text , jqXHR.responseText)

exports.gui.GUI = GUI
