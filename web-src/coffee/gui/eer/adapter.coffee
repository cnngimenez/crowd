# adapter.coffee --
# Copyright (C) 2018 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


exports = exports ? this
exports.gui = exports.gui ? {}
exports.gui.eer = exports.gui.eer ? {}


# @namespace gui.eer
#
# DiagAdapter for ERD diagrams.
class EERAdapter extends gui.DiagAdapter
    constructor: (diag, paper) ->
        super(diag, paper)

    add_attribute: (hash_data) ->
        @diag.add_attribute(hash_data)

    #
    # Add an Entity to the diagram.
    #
    # @param hash_data {Hash} data information for creating the Class. Use `name`, `attribs` and `methods` keys.
    # @see Class
    # @see Diagram#add_class
    add_object_type: (hash_data) ->
        @diag.add_entity(hash_data)

    #
    # Add a simple relationships from A to B.
    # Then, set the selection state for restoring the interface.
    #
    # @example Getting a classid
    #   < graph.getCells()[0].id
    #   > "5777cd89-45b6-407e-9994-5d681c0717c1"
    #
    # @param class_a_id {string}
    # @param class_b_id {string}
    # @param name {string} optional. The association name.
    # @param mult {array} optional. An array of two string with the cardinality from class and to class b.
    # @param roles {array} optional.
    add_relationship: (class_a_id, class_b_id, name=null, mult=null, roles=null) ->
        @diag.add_association(class_a_id, class_b_id, name, mult, roles)
        gui.gui_instance.current_gui.set_selection_state()
        gui.gui_instance.current_gui.clear_relationship()
        gui.gui_instance.current_gui.widgets.hide_options()

    add_relationship_attr: (class_id, attribute_id, name=null)->
        @diag.add_relationship_attr(class_id, attribute_id, name)

    # # Possibly creates a link between a class and an IS-A (one line only).
    # add_relationship_isa: (class_id, isa_id, name) ->
    # 	@diag.add_relationship_isa(class_id, isa_id, name)

    # # Possibly creates a link between an IS-A and a class (one line only).
    # add_relationship_isa_inverse: (class_id, isa_id, name=null)->
    # 	@diag.add_relationship_isa_inverse(isa_id, class_id, name)

    # Add Isa links and then set the selection state.
    #
    # @param class_parent_id {string} The parent class Id.
    # @param class_child_id {string} The child class Id.
    #
    # @todo Support various children on parameter class_child_id.
    add_subsumption: (class_parent_id, class_child_id, disjoint=false, covering=false) ->
        @diag.add_generalization(class_parent_id, class_child_id, disjoint, covering)
        isa_id = @diag.get_last_isa_by_id()
        @diag.add_relationship_isa(class_parent_id, isa_id)
        @diag.add_relationship_isa_inverse(isa_id, class_child_id)
        gui.gui_instance.current_gui.set_selection_state()
        gui.gui_instance.current_gui.widgets.hide_options()

    #
    # Delete a class from the diagram.
    #
    # @param class_id {string} a String with the class Id.
    delete_attr: (attr_id) ->
        @diag.delete_attr_by_attrid(attr_id)


# Unique instance of EERAdapter.
# 
# @namespace gui.eer
exports.gui.eer.ieeradapter = null

exports.gui.eer.EERAdapter = EERAdapter
