# selectionstate.coffee --
# Copyright (C) 2017 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.gui = exports.gui ? {}


# Selection state, the user can select some classes.
#
# Example of usage:
#
# ```coffee
# 
# ```
# 
# @namespace gui
class SelectionState extends gui.State
   
    constructor: () ->
        super()
        @selected_cells = []   

    # Activate this state reseting all.
    #
    # Callback functions can be passed as parameter. Default to:
    # 
    # ```coffee
    # callback =
    #     cell_clicked: null
    # ```
    #
    # @param callback {object} Callback functions. See State.callbacks variable.
    activate: (callbacks = null) ->
        if callbacks?
            @callbacks = callbacks
        @selected_cells = []

    # Highlight the cell and call the @callback.cell_clicked function.
    #
    # Also register the cell if it is highlighted.
    on_cell_clicked: (cellView, event, x, y, gui_instance) ->
        super()
        if (cellView.highlighted == undefined or cellView.highlighted == false)
            cellView.highlight()
            cellView.highlighted = true
            @selected_cells.push cellView
        else
            cellView.unhighlight()
            cellView.highlighted = false
            @selected_cells.pop cellView


exports.gui.SelectionState = SelectionState
