# GUIState.coffee --
# Copyright (C) 2016 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


exports = exports ? this
exports.gui = exports.gui ? {}

# Abstract class that helps determine how the interface
# should respond to a user action depending on the current
# state.
#
# # Callbacks
# Callbacks are setted using the callbacks object:
#
# ```coffeescript
# istate = new State()
# istate.callbacks =
#     cell_clicked: (cell) ->
#         console.log "callback works"
# ```
# 
# @abstract
# @namespace gui
class State
    constructor: () ->
        @selectionstate_inst = null
        @associationstate_inst = null
        @isastate_inst = null

        @callbacks =
            cell_clicked: null            

    # What to do when the user clicked on a cell.
    #
    # @abstract
    on_cell_clicked: (cell_view, event, x, y, gui_instance) ->
        if @callbacks.cell_clicked?
            @callbacks.cell_clicked cell_view, event, x, y

    # Return the selection state for selecting various cells.
    selection_state: () ->
        if not @selectionstate_inst?
            @selectionstate_inst = new gui.SelectionState()
        @selectionstate_inst

    association_state: () ->
        if not @associationstate_inst?
            @associationstate_inst = new gui.AssociationState()
        @associationstate_inst

    isa_state: () ->
        if not @isastate_inst?
            @isastate_inst = new gui.IsAState()
        @isastate_inst

exports.gui.State = State
