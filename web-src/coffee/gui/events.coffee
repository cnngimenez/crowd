# events.coffee --
# Copyright (C) 2018 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


exports = exports ? this
exports.gui ? exports.gui ? {}

# @namespace gui
#
# Events callbacks for a GUI.
class Events
    constructor: () ->
    # What to do when a Joint cell is clicked.
    #
    # Follows the responsability to the @current_gui.
    #
    # @param cellview [dia.CellView] The Joint CellView that recieves the click event.
    # @param event The event descripiton object
    # @param x [number] X coordinate.
    # @param y [number] Y coordinate.
    on_cell_clicked: (cellview, event, x, y) ->

