<?php
/*

   Copyright 2018 GILIA

   Author: Giménez, Christian. Braun, Germán

   graphicalrules.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Wicom\Translator\Strategies\GOMS;


load("graphicalrules.php");

use \XMLReader;
use \SimpleXMLElement;
use \SimpleXMLIterator;
use \XMLWriter;

/**
  This class contains xpath rules to look for graphical primitives from an owl 2 ontology and
  crowd decoding strategy

  protected $sub = [];
  protected $sub_total = [];
  protected $sub_partial = [];
  protected $sub_disj = [];
  protected $rel_without_class = [];
  protected $equivalence = [];
  protected $disjointness = [];

*/
class UMLGraphicalRules{

    function __construct($owl2){
      $this->xml = new SimpleXMLElement($owl2);

      $this->class = "//*[local-name()='Ontology']/*[local-name()='SubClassOf']/
                            *[local-name()='Class'][2][@abbreviatedIRI='owl:Thing']/
                            ../*[local-name()='Class'][1]";

      $this->sub = "//*[local-name()='Ontology']/*[local-name()='SubClassOf']/
                            *[local-name()='Class'][2][@IRI]/
                            ../*[local-name()='Class'][1][@IRI]/.. |
                            //*[local-name()='Ontology']/*[local-name()='SubClassOf']/
                              *[local-name()='Class'][1][@IRI]/
                              ../*[local-name()='Class'][2][@IRI]/..";

      $this->sub_partial = "";
      $this->sub_total = "";
/*
      $this->sub_total = ['/SubClassOf/Class/', 'SubClassOf/ObjectUnionOf/'];
      $this->sub_disj = ['/SubClassOf/Class/', 'SubClassOf/ObjectUnionOf/', '/DisjointClasses/Class/'];
      $this->$rel_without_class_0N = ['/SubClassOf/ObjectSomeValuesFrom/ObjectProperty/',
                                      '/SubClassOf/ObjectSomeValuesFrom/ObjectProperty/ObjectInverseOf/',
                                      '/EquivalentClasses'];*/

    }

    /**
    XPath for searching classes
    */
    function search_classes(){
      $classes = $this->xml->xpath($this->class);
      $classes_dec = [];

      foreach ($classes as $class){
        array_push($classes_dec,["classA" => $class["IRI"]->__toString(),
                                 "classB" => "owl:Thing"]);
      }
      return json_encode($classes_dec);
    }

    /**
    XPath for searching UML subsumptions
    @return json representing subsumption {"classA":ClassA, "classB":ClassB}.
    */
    function search_sub(){
      $subs = $this->xml->xpath($this->sub);
      $subs_dec = [];

      $num = 0;
      while ($num <= count($subs)-1){
        $classes = $subs[$num]->children();

        array_push($subs_dec,["classA" => $classes[0]["IRI"]->__toString(),
                              "classB" => $classes[1]["IRI"]->__toString()]);
        $num++;
      }
      return json_encode($subs_dec);
    }

    /**
    XPath for searching UML partial subsumptions
    @param $parent {String}. Common parent for the composite subsumption.
    @return json representing union of classes.

    <SubClassOf>
      <ObjectUnionOf>
          <Class IRI="#B"/>
          <Class IRI="#C"/>
      </ObjectUnionOf>
      <Class IRI="#A"/>
    </SubClassOf>
    */
    function search_subpartial($parent){
      $xpath = "//*[local-name()='Ontology']/*[local-name()='SubClassOf']/
                *[local-name()='Class'][@IRI='".$parent."']/../*[local-name()='ObjectUnionOf']/..";
      $partial = $this->xml->xpath($xpath);

      $c_partial = [];

      foreach($partial as $elem){
        $string = $elem->asXML();
        $xml = new SimpleXMLIterator($string);
        $xml->rewind();
        $first_tag = $xml->children();
        $union = [];
        $class = [];

        foreach ($first_tag as $child){
          $name = $child->getName();

          switch ($name){
            case "ObjectUnionOf" :
                $union_child = $child->children();

                foreach ($union_child as $last){
                  array_push($union, $last["IRI"]->__toString());
                }
              break;

            case "Class" :
                array_push($class,$child["IRI"]->__toString());
              break;
          }
        }
        array_push($c_partial,["union" => $union,"class" => $class]);
      }
      return json_encode($c_partial);
    }

    /**
    XPath for searching UML total subsumptions
    @param $parent {String}. Common parent for the composite subsumption.

    <SubClassOf>
        <ObjectUnionOf>
            <Class IRI="#B"/>
            <Class IRI="#C"/>
        </ObjectUnionOf>
        <Class IRI="#A"/>
    </SubClassOf>
    <SubClassOf>
        <Class IRI="#A"/>
        <ObjectUnionOf>
            <Class IRI="#B"/>
            <Class IRI="#C"/>
        </ObjectUnionOf>
    </SubClassOf>
    */
    function search_subtotal($parent){
      $xpath = "//*[local-name()='Ontology']/*[local-name()='SubClassOf']/
                *[local-name()='Class'][2][@IRI='".$parent."']/../*[local-name()='ObjectUnionOf']/.. |
              //*[local-name()='Ontology']/*[local-name()='SubClassOf']/
                *[local-name()='Class'][1][@IRI='".$parent."']/../*[local-name()='ObjectUnionOf']/..";
      $t = $this->xml->xpath($xpath);
      $c_total = [];

      foreach($t as $elem){
         $string = $elem->asXML();

         $xml = new SimpleXMLIterator($string);
         $xml->rewind();
         $first_tag = $xml->children();
         $total = [];
         $class = [];

         foreach ($first_tag as $child){
            $name = $child->getName();

            switch ($name){
              case "ObjectUnionOf" :
                  $union_child = $child->children();

                  foreach ($union_child as $last){
                    array_push($total, $last["IRI"]->__toString());
                  }
                 break;

               case "Class" :
                      array_push($class,$child["IRI"]->__toString());
                 break;
            }
        }
        array_push($c_total,["total" => $total,"class" => $class]);
      }
      return json_encode($c_total);
    }

    /**
    XPath for searching disjointness. Note that this OWL 2 primitives cannot be drawn in UML.
    However, we take them in order to check non-graphicable links and disjoint composite subsumptions.

    @param $classA {String}. Optional. Class.
    @param $classB {String}. Optional. Class.

    <DisjointClasses>
      <Class IRI="#B"/>
      <Class IRI="#C"/>
    </DisjointClasses>
    <DisjointClasses>
      <Class IRI="#C"/>
      <Class IRI="#A"/>
    </DisjointClasses>
    */
    function search_disjointness($classA = '', $classB = ''){

      if (($classA != '') && ($classB != '')){
        $xpath = "//*[local-name()='Ontology']/*[local-name()='DisjointClasses']/
                    *[local-name()='Class'][1][@IRI='".$classA."']/../
                    *[local-name()='Class'][2][@IRI='".$classB."']/..";
      }
      else {
        $xpath = "//*[local-name()='Ontology']/*[local-name()='DisjointClasses']/
                    *[local-name()='Class'][1][@IRI]/../
                    *[local-name()='Class'][2][@IRI]/..";
      }

      $d = $this->xml->xpath($xpath);
      $disjoint = [];

      foreach($d as $elem){
         $string = $elem->asXML();
         $xml = new SimpleXMLIterator($string);
         $xml->rewind();
         $tags = $xml->children();
         $classes = [];

         foreach ($tags as $class){
           array_push($classes, $class["IRI"]->__toString());
         }
         array_push($disjoint,["disjoint" => $classes]);
      }

      return json_encode($disjoint);
    }

    /**
    XPath for searching equivalence. Note that this OWL 2 primitives cannot be drawn in UML.
    However, we take them in order to check non-graphicable links.

    @param $classA {String}. Optional. Class.
    @param $classB {String}. Optional. Class.

    <EquivalentClasses>
      <Class IRI="#B"/>
      <Class IRI="#C"/>
    </EquivalentClasses>
    <EquivalentClasses>
      <Class IRI="#C"/>
      <Class IRI="#A"/>
    </EquivalentClasses>
    */
    function search_equivalence($classA = '', $classB = ''){

      if (($classA != '') && ($classB != '')){
        $xpath = "//*[local-name()='Ontology']/*[local-name()='EquivalentClasses']/
                    *[local-name()='Class'][1][@IRI='".$classA."']/../
                    *[local-name()='Class'][2][@IRI='".$classB."']/..";
      }
      else {
        $xpath = "//*[local-name()='Ontology']/*[local-name()='EquivalentClasses']/
                    *[local-name()='Class'][1][@IRI]/../
                    *[local-name()='Class'][2][@IRI]/..";
      }

      $d = $this->xml->xpath($xpath);
      $eq = [];

      foreach($d as $elem){
         $string = $elem->asXML();
         $xml = new SimpleXMLIterator($string);
         $xml->rewind();
         $tags = $xml->children();
         $classes = [];

         foreach ($tags as $class){
           array_push($classes, $class["IRI"]->__toString());
         }
         array_push($eq,["equivalent" => $classes]);
      }

      return json_encode($eq);
    }

}
