<?php
/*

   Copyright 2018 GILIA

   Author: Giménez, Christian. Braun, Germán

   graphicalrules.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Wicom\Translator\Strategies\GOMS;


load("owllinkbuilder.php", "../../../builders/");
load("documentbuilder.php", "../../../builders/");
load("owlbuilder.php", "../../../builders/");

use Wicom\Translator\Builders\OWLlinkBuilder;
use Wicom\Translator\Builders\OWLBuilder;
use Wicom\Translator\Builders\DocumentBuilder;
use \XMLReader;
use \SimpleXMLElement;
use \SimpleXMLIterator;
use \XMLWriter;

/**
  This class contains xpath rules to look for graphical primitives from an owl 2 ontology
*/
class GraphicalRule{

    protected $subsumption = [];
    protected $relationship = [];
    protected $equivalence = [];
    protected $disjointness = [];

    function __construct(){
    }

    function get_subsumption_rule(){
        return $this->subsumption;
    }

    function get_relationship_rule(){
        return $this->relationship;
    }

    function get_equivalence_rule(){
        return $this->equivalence;
    }

    function get_disjoint_rule(){
        return $this->equivalence;
    }

    
}
