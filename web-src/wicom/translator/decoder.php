<?php
/*

   Copyright 2018 GILIA

   Author: GILIA

   decoder.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Wicom\Decoder;

use function \load;

use function \json_decode;

/**
   Decode an Ontology OWL to be drawn in crowd using diagram UML/EER/ORM according to an encoding strategy.

   1. Give a Strategy for specifying the algorithm for decoding the OWL 2.
   2. Give a Builder for specifying the UML/EER/ORM json input format.

   # JSON Format

   We expect the following fields:

   - `classes` : An Array of classes information. Each class should have:
     - `attrs` An array of strings representing all attributes names
     - `methods` An array of strings representing all attributes names
     - `name` A string which represent the name of the class.
   - links : An array of links information. Each link should have:
     - `classes` : An array of strings with the name of the classes involved on the relationship.
     - `multiplicity` : An array of strings with the multiplicity on each class side.
     - `name` : A string with the name of the link.
     - `type` : A string with the type name of the link. Could be: "association", "generalization".

   ## Example
   @code{json}
   {"classes": [
     {"attrs":[], "methods":[], "name": "Person"},
     {"attrs":[], "methods":[], "name": "Cellphones"}],
    "links": [
     {"classes": ["Person", "Cellphones"],
      "multiplicity": ["1..1", "1..*"],
      "name": "hasCellphone",
      "type": "association"}
      ]
   }
   @endcode

 */
class Decoder{
    protected $strategy = null;
    protected $jsonbuilder = null;

    function __construct($strategy, $jsonbuilder){
        $this->strategy = $strategy;
        $this->jsonbuilder = $builder;
    }

    /**
       @param owl2 An OWL 2 Ontology.
       @return an UML Json.
     */
    function to_umljson($owl2){
        $this->strategy->decode($owl2, $this->builder);

        $document = $this->builder->get_product();
        return $document->to_string();
    }



    /**
       @param json A String.
       @return a XML OWL2 String.
     */
    function to_owl2($json){
        $json_obj = json_decode($json, true);
        $this->builder->insert_header_owl2();

        $this->strategy->translate($json, $this->builder);

/*        if (array_key_exists("owllink", $json_obj)){
            $this->builder->insert_owllink($json_obj["owllink"]);
        }*/

        $this->builder->insert_footer();
        $document = $this->builder->get_product();
        return $document->to_string();
    }

}
