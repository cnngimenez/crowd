<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Wicom\Translator\Verbalisation;


class Verbalisation{
        
    public $verbalisation;    
    
    function __construct(){
        $this->verbalisation= "";
	}
        
    function generate_verbalisation($fol_str){
        $fol = json_decode($fol_str, true); 
        
        $this->verbalisationOfClass($fol);
        $this->verbalisationOfSubsumptions($fol);
        $this->verbalisationOfAssociation($fol);        
    }
    
    function verbalisationOfClass($fol){         
        $js_classes = $fol["Classes"];
        $js_attributes=$fol["Attribute"];
        
        foreach ($js_classes as $class_pred) {     
            $nameClass=$class_pred["forall"]["pred"]["name"];
            
            $attributes= array_filter($js_attributes,function($attrib) use ($nameClass) {return $attrib["forall"]["imply"]["and"]["pred"]["name"] == $nameClass;});
            
            $this->verbalisation= $this->verbalisation."\n".$this->verbalisationOfClassAndAtributtes($nameClass, $attributes);   
        }
    }
    
    function verbalisationOfSubsumptions($fol){ 
        $js_subsumptions = $fol["IsA"];
        $disj_and_cover=0; // count of  constrains (==2 is disjoint and covering)
        
         foreach ($js_subsumptions as $key=>$value) {
             if(array_key_exists("forall", $value)){
                 $this->verbalisationIsA($value);
             }
             else{
                 if(array_key_exists("disjoint", $value)){
                     $this->verbalisationOfDisjointConstraint($value);
                     $disj_and_cover++;
                 }
                 if(array_key_exists("covering",$value)){
                     $disj_and_cover++;
                     $this->verbalisationOfCoveringConstraint($value,$disj_and_cover);
                 }
             }
             
         }
    }
    
    private function verbalisationIsA($subsumption) {
        $parent = $subsumption["forall"]["imply"]["predB"]["name"];
        $child = $subsumption["forall"]["imply"]["pred"]["name"];

        $parent_gender = "Un/Una ";
        $child_gender = "Un/Una ";

        $subsumption_verb = $child_gender . " " . $child . " es " . $parent_gender . " " . $parent.".";

        $this->verbalisation = $this->verbalisation . "\n" . $subsumption_verb;
    }

    private function verbalisationOfDisjointConstraint($constraint) {
        $gender_sub1 = "Ningún/a ";
        $gender_sub2 = "un/a ";
        
        $disjoints_classes = $constraint["disjoint"];

        foreach ($disjoints_classes as $disjoint) {
            $sub1 = $disjoint["forall"]["imply"]["pred"]["name"];
            $sub2 = $disjoint["forall"]["imply"]["neg"]["pred"]["name"];

            $constraint_verb = $gender_sub1 . $sub1 . " puede ser " . $gender_sub2 . $sub2;

            $this->verbalisation = $this->verbalisation . "\n" . $constraint_verb.".";
        }
    }

    private function verbalisationOfCoveringConstraint($constraint,$disj_and_cover){
        $coverings_classes=$constraint["covering"];
        
        foreach($coverings_classes as $covering){
            
            $parent=$covering["forall"]["imply"]["pred"]["name"];
            
            $or_classes=$covering["forall"]["imply"]["or"];
            
            $constraint_verb="Cada ".$parent. " debe ser al menos ";
            
            foreach($or_classes as $or_class){
                $class=$or_class["pred"]["name"];
                
                if($or_class==end($or_classes)){
                    $constraint_verb=$constraint_verb."o ".$class;
                }
                else{
                    $constraint_verb=$constraint_verb.$class.", ";
                }                                
            }
            
            $this->verbalisation= $this->verbalisation."\n".$constraint_verb.".";
            
            if($disj_and_cover==2){
                $this->verbalisation= $this->verbalisation.", pero no todos (solo uno de ellos).";
            }
        }
    }
    
    function verbalisationOfAssociation($fol){
        $js_associations=$fol["Links"];
        foreach($js_associations as $association){ //$association is [3]----> pred1,pred2,pred3
            
            $assoc_name=$association[0]["forall"]["imply"]["pred"]["name"];
            $classA=$association[0]["forall"]["imply"]["and"]["pred"]["name"];
            $classB=$association[0]["forall"]["imply"]["and"]["predB"]["name"];
            
            //next($js_associations);
            
            $multiplicity1=[];
            
            $multiplicity1[0]=$association[1]["forall"]["imply"]["multiplicity"]["min"];
            $multiplicity1[1]=$association[1]["forall"]["imply"]["multiplicity"]["max"];
            $rolA_name=$association[1]["forall"]["imply"]["multiplicity"]["#"]["pred"]["name"];
            
            //next($js_associations);
            
            $multiplicity2=[];
            $multiplicity2[0]=$association[2]["forall"]["imply"]["multiplicity"]["min"];
            $multiplicity2[1]=$association[2]["forall"]["imply"]["multiplicity"]["max"];
            $rolB_name=$association[2]["forall"]["imply"]["multiplicity"]["#"]["pred"]["name"];
            
            $verbalisation1=$this->verbalisationOfAssociationWithMultiplicities($classA, $classB, $assoc_name, $rolA_name, $rolB_name, $multiplicity1);
            $verbalisation2=$this->verbalisationOfAssociationWithMultiplicities($classB, $classA, $assoc_name, $rolA_name, $rolB_name, $multiplicity2);
            
            $this->verbalisation= $this->verbalisation."\n".$verbalisation1.".\n".$verbalisation2.".";
            
        }
    }
    
    private function verbalisationOfClassAndAtributtes($class_name,$attributes){
        $class_gender="Un/Una ";
        $has="tiene ";
        
        if(!empty($attributes)){
            $verbalisation=$class_gender." ".$class_name." ". $has;
        
            foreach ($attributes as $attribute){      
                $attrib_gender="un/una ";
                $attribute_name= $attribute["forall"]["imply"]["and"]["predA"]["name"];
                
                $aux= $attrib_gender . $attribute_name;
                
                if($attribute == end($attributes)){
                    if($attribute== reset($attributes)){
                        $verbalisation=$verbalisation. " ". $aux.".";
                    }
                    else{
                        $verbalisation=$verbalisation. "y ". $aux.".";
                    }
                    
                }
                else{
                    $verbalisation=$verbalisation. $aux.", ";
                }
            }
            return $verbalisation;
        }
    }
    
    private function verbalisationOfAssociationWithMultiplicities($classA, $classB, $association, $roleA, $roleB, $mult){
        $class_gender="Cada ";
        
        $verbalisation=$class_gender.$classA." ".$association;
   
        $mult_verbA= $this->verbalizeMultiplicity($mult);
  
        $verbalisation=$verbalisation." ".$mult_verbA." ".$classB;
        
        return $verbalisation;
    }
    
    private function verbalizeMultiplicity($mult){
        $mult_verb="";
       
        if($mult[0]!="*"){ //is a number
            $mult_verb=$this->convertNumber((int)$mult[0]);
            if($mult[1]!="*"){
                $mult_verb=$mult_verb." a ".$this->convertNumber((int)$mult[1]);
            }
            else{ //is "*"
                $mult_verb=$mult_verb." o más ";
            }
        }
        return $mult_verb;
    }
    
    private function convertNumber($numero) {
        $valor = array ('cero','uno', 'dos', 'tres', 'cuatro', 'cinco', 'seis', 'siete', 'ocho', 'nueve', 'diez');
        //return $valor[$numero - 1];
        return $valor[$numero];
    }

}