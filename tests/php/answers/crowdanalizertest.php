<?php
/*

   Copyright 2016 GILIA

   Author: Giménez, Christian. Braun, Germán

   berardianalizertest.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once("common.php");

// use function \load;
load("crowdanalizer.php", "wicom/translator/strategies/qapackages/answeranalizers/");

use Wicom\Translator\Strategies\QAPackages\AnswerAnalizers\CrowdAnalizer;

class CrowdAnalizerTest extends PHPUnit\Framework\TestCase
{

/*    public function testFilterXML(){
        $query_input = <<<'EOT'
<?xml version="1.0" encoding="UTF-8"?>
<RequestMessage xmlns="http://www.owllink.org/owllink#"
		xmlns:owl="http://www.w3.org/2002/07/owl#"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="http://www.owllink.org/owllink#
				    http://www.owllink.org/owllink-20091116.xsd">
  <CreateKB kb="http://localhost/kb1" />
  <Tell kb="http://localhost/kb1">
    <owl:SubClassOf>
      <owl:Class IRI="Person" />
      <owl:Class abbreviatedIRI="owl:Thing" />
    </owl:SubClassOf>
    <owl:SubClassOf>
      <owl:Class IRI="OtherPerson" />
      <owl:Class abbreviatedIRI="owl:Thing" />
    </owl:SubClassOf>
    <owl:SubClassOf>
      <owl:Class IRI="Nope this one nope" />
      <owl:Class abbreviatedIRI="owl:Thing" />
    </owl:SubClassOf>
  </Tell>

  <!-- Queries -->

  <IsKBSatisfiable kb="http://localhost/kb1" />
  <IsClassSatisfiable kb="http://localhost/kb1">
    <owl:Class IRI="Person" />
  </IsClassSatisfiable>
  <IsClassSatisfiable kb="http://localhost/kb1">
    <owl:Class IRI="OtherPerson" />
  </IsClassSatisfiable>
  <IsClassSatisfiable kb="http://localhost/kb1">
    <owl:Class IRI="Nope this one nope" />
  </IsClassSatisfiable>



  <ReleaseKB kb="http://localhost/kb1" />
</RequestMessage>
EOT;
        $answer_output = <<<'EOT'
<?xml version="1.0" encoding="UTF-8"?>
<ResponseMessage xmlns="http://www.owllink.org/owllink#"
                 xmlns:owl="http://www.w3.org/2002/07/owl#">
  <KB kb="http://localhost/kb1"/>
  <OK/>
  <BooleanResponse result="true"/>
  <BooleanResponse result="true"/>
  <BooleanResponse result="true"/>
  <BooleanResponse result="false"/>
  <OK/>
</ResponseMessage>
EOT;

        $expected = [ "IsKBSatisfiable" => "true",
                      "IsClassSatisfiable" => [
                          ["true", "Person"],
                          ["true", "OtherPerson"],
                          ["false", "Nope this one nope"]
                      ]
        ];

        $oa = new CrowdAnalizer();
        $oa->generate_answer($query_input, $answer_output);
        $actual = $oa->filter_xml();

        $this->assertEquals($expected, $actual, true);
    }

    public function testAnswerJson(){
        $query_input = <<<'EOT'
<?xml version="1.0" encoding="UTF-8"?>
<RequestMessage xmlns="http://www.owllink.org/owllink#"
		xmlns:owl="http://www.w3.org/2002/07/owl#"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="http://www.owllink.org/owllink#
				    http://www.owllink.org/owllink-20091116.xsd">
  <CreateKB kb="http://localhost/kb1" />
  <Tell kb="http://localhost/kb1">
    <owl:SubClassOf>
      <owl:Class IRI="Person" />
      <owl:Class abbreviatedIRI="owl:Thing" />
    </owl:SubClassOf>
    <owl:SubClassOf>
      <owl:Class IRI="OtherPerson" />
      <owl:Class abbreviatedIRI="owl:Thing" />
    </owl:SubClassOf>
    <owl:SubClassOf>
      <owl:Class IRI="Nope this one nope" />
      <owl:Class abbreviatedIRI="owl:Thing" />
    </owl:SubClassOf>
  </Tell>

  <!-- Queries -->

  <IsKBSatisfiable kb="http://localhost/kb1" />
  <IsClassSatisfiable kb="http://localhost/kb1">
    <owl:Class IRI="Person" />
  </IsClassSatisfiable>
  <IsClassSatisfiable kb="http://localhost/kb1">
    <owl:Class IRI="OtherPerson" />
  </IsClassSatisfiable>
  <IsClassSatisfiable kb="http://localhost/kb1">
    <owl:Class IRI="Nope this one nope" />
  </IsClassSatisfiable>



  <ReleaseKB kb="http://localhost/kb1" />
</RequestMessage>
EOT;
        $answer_output = <<<'EOT'
<?xml version="1.0" encoding="UTF-8"?>
<ResponseMessage xmlns="http://www.owllink.org/owllink#"
                 xmlns:owl="http://www.w3.org/2002/07/owl#">
  <KB kb="http://localhost/kb1"/>
  <OK/>
  <BooleanResponse result="true"/>
  <BooleanResponse result="true"/>
  <BooleanResponse result="true"/>
  <BooleanResponse result="false"/>
  <OK/>
</ResponseMessage>
EOT;

        $expected = <<<'EOT'
{"satisfiable":{"kb":true,"classes":["Person","OtherPerson"]},"unsatisfiable":{"classes":["Nope this one nope"]},"graphical_suggestions":{"links":[]},"non_graphical_suggestion":{"links":[]},"reasoner":{"input":"","output":""}}
EOT;

        $oa = new CrowdAnalizer();
        $oa->generate_answer($query_input, $answer_output);
        $oa->analize();
        $answer = $oa->get_answer();

        // Removing input and output XML string, is merely descriptive.
        $answer->set_reasoner_input("");
        $answer->set_reasoner_output("");
        $actual = $answer->to_json();


        print("\n\n");
        print($actual);
        print("\n\n");
        print("\n\n");
        print($expected);
        print("\n\n");

        $this->assertJsonStringEqualsJsonString($expected, $actual, true);
    }

    public function testSimpleOWLlink(){
        $query_input = <<<'EOT'
<?xml version="1.0" encoding="UTF-8"?>
<RequestMessage xmlns="http://www.owllink.org/owllink#"
		xmlns:owl="http://www.w3.org/2002/07/owl#"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="http://www.owllink.org/owllink#
				    http://www.owllink.org/owllink-20091116.xsd">
  <CreateKB kb="http://localhost/kb1" />
  <Tell kb="http://localhost/kb1">
    <owl:SubClassOf>
      <owl:Class IRI="Person" />
      <owl:Class abbreviatedIRI="owl:Thing" />
    </owl:SubClassOf>
  </Tell>

  <!-- Queries -->

  <IsKBSatisfiable kb="http://localhost/kb1" />
  <IsClassSatisfiable kb="http://localhost/kb1">
    <owl:Class IRI="Person" />
  </IsClassSatisfiable>

  <ReleaseKB kb="http://localhost/kb1" />
</RequestMessage>
EOT;
        $answer_output = <<<'EOT'
<?xml version="1.0" encoding="UTF-8"?>
<ResponseMessage xmlns="http://www.owllink.org/owllink#"
                 xmlns:owl="http://www.w3.org/2002/07/owl#">
  <KB kb="http://localhost/kb1"/>
  <OK/>
  <BooleanResponse result="true"/>
  <BooleanResponse result="true"/>
  <OK/>
</ResponseMessage>
EOT;

        $expected = <<<'EOT'
       {
           "satisfiable": {
               "kb" : true,
               "classes" : ["Person"]
           },
           "unsatisfiable": {
              	"classes" : []
           },
           "suggestions" : {
              	"links" : []
           },
           "reasoner" : {
              	"input" : "",
              	"output" : ""
           }
       }
EOT;

        $oa = new CrowdAnalizer();
        $oa->generate_answer($query_input, $answer_output);
        $oa->analize();
        $answer = $oa->get_answer();
        // Removing input and output XML string, is merely descriptive.
        $answer->set_reasoner_input("");
        $answer->set_reasoner_output("");
        $actual = $answer->to_json();


        $this->assertJsonStringEqualsJsonString($expected, $actual, true);
    }
    public function testRealCase(){
        $query_input = <<<'EOT'
<?xml version="1.0" encoding="UTF-8"?>
<RequestMessage xmlns="http://www.owllink.org/owllink#" xmlns:owl="http://www.w3.org/2002/07/owl#" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.owllink.org/owllink# http://www.owllink.org/owllink-20091116.xsd"><CreateKB kb="http://localhost/kb1"/><Tell kb="http://localhost/kb1"><owl:SubClassOf><owl:Class IRI="Hi World"/><owl:Class abbreviatedIRI="owl:Thing"/></owl:SubClassOf></Tell><IsKBSatisfiable kb="http://localhost/kb1"/><IsClassSatisfiable kb="http://localhost/kb1"><owl:Class IRI="Hi World"/></IsClassSatisfiable></RequestMessage>
EOT;
        $answer_output = <<<'EOT'
<?xml version="1.0" encoding="UTF-8"?><ResponseMessage xmlns="http://www.owllink.org/owllink#"                 xmlns:owl="http://www.w3.org/2002/07/owl#">  <KB kb="http://localhost/kb1"/>  <OK/>  <BooleanResponse result="true"/>  <BooleanResponse result="true"/></ResponseMessage>
EOT;

        $expected = <<<'EOT'
       {
           "satisfiable": {
               "kb" : true,
               "classes" : ["Hi World"]
           },
           "unsatisfiable": {
              	"classes" : []
           },
           "suggestions" : {
              	"links" : []
           },
           "reasoner" : {
              	"input" : "",
              	"output" : ""
           }
       }
EOT;

        $oa = new CrowdAnalizer();
        $oa->generate_answer($query_input, $answer_output);
        $oa->analize();
        $answer = $oa->get_answer();
        // Removing input and output XML string, is merely descriptive.
        $answer->set_reasoner_input("");
        $answer->set_reasoner_output("");
        $actual = $answer->to_json();



        $this->assertJsonStringEqualsJsonString($expected, $actual, true);
    }

    public function testUnsatisfiableOWLlink(){
        $query_input = <<<'EOT'
<?xml version="1.0" encoding="UTF-8"?>
<RequestMessage xmlns="http://www.owllink.org/owllink#" xmlns:owl="http://www.w3.org/2002/07/owl#" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.owllink.org/owllink# http://www.owllink.org/owllink-20091116.xsd"><CreateKB kb="http://localhost/kb1"/><Tell kb="http://localhost/kb1"><owl:SubClassOf><owl:Class IRI="Test Class"/><owl:Class abbreviatedIRI="owl:Thing"/></owl:SubClassOf></Tell><Tell kb="http://localhost/kb1">
<owl:SubClassOf>
  <owl:Class IRI="Test Class" />
  <owl:ObjectComplementOf>
    <owl:Class IRI="Test Class" />
  </owl:ObjectComplementOf>
</owl:SubClassOf>
</Tell><IsKBSatisfiable kb="http://localhost/kb1"/><IsClassSatisfiable kb="http://localhost/kb1"><owl:Class IRI="Test Class"/></IsClassSatisfiable></RequestMessage>
EOT;
        $answer_output = <<<'EOT'
<?xml version="1.0" encoding="UTF-8"?>
<ResponseMessage xmlns="http://www.owllink.org/owllink#"
                 xmlns:owl="http://www.w3.org/2002/07/owl#">
  <KB kb="http://localhost/kb1"/>
  <OK/>
  <OK/>
  <BooleanResponse result="true"
                   warning="Unsatisfiable classes: (*BOTTOM* BOTTOM file://owllink-unsatisfiable.xmlTest Class)"/>
  <BooleanResponse result="false"/>
</ResponseMessage>
EOT;

        $expected = <<<'EOT'
       {
           "satisfiable": {
               "kb" : true,
               "classes" : []
           },
           "unsatisfiable": {
              	"classes" : ["Test Class"]
           },
           "suggestions" : {
              	"links" : []
           },
           "reasoner" : {
              	"input" : "",
              	"output" : ""
           }
       }
EOT;

        $oa = new CrowdAnalizer();
        $oa->generate_answer($query_input, $answer_output);
        $oa->analize();
        $answer = $oa->get_answer();
        // Removing input and output XML string, is merely descriptive.
        $answer->set_reasoner_input("");
        $answer->set_reasoner_output("");
        $actual = $answer->to_json();



        $this->assertJsonStringEqualsJsonString($expected, $actual, true);
    }


*/


    public function testAnswerOWLlinkOutput(){
        $query_input = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
        <RequestMessage xmlns="http://www.owllink.org/owllink#"
                        xmlns:owl="http://www.w3.org/2002/07/owl#"
                        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                        xsi:schemaLocation="http://www.owllink.org/owllink# http://www.owllink.org/owllink-20091116.xsd"
        				xml:base="">
                <CreateKB kb="http://localhost/kb1"/>
                <Tell kb="http://localhost/kb1">


                    <owl:SubClassOf>
                      <owl:Class IRI="D"/>
                      <owl:Class IRI="A"/>
                    </owl:SubClassOf>

                    <owl:SubClassOf>
                      <owl:Class IRI="C"/>
                      <owl:Class IRI="A"/>
                    </owl:SubClassOf>

                    <owl:SubClassOf>
                      <owl:Class IRI="B"/>
                      <owl:Class IRI="A"/>
                    </owl:SubClassOf>

                    <owl:SubClassOf>
                      <owl:Class IRI="A"/>
                      <owl:Class abbreviatedIRI="owl:Thing"/>
                    </owl:SubClassOf>


                    <owl:DisjointClasses>
                      <owl:Class IRI="D"/>
                      <owl:Class IRI="B"/>
                    </owl:DisjointClasses>

        		    <owl:EquivalentClasses>
                		<owl:Class IRI="A"/>
                		<owl:ObjectUnionOf>
                    		<owl:Class IRI="B"/>
                    		<owl:Class IRI="C"/>
                		</owl:ObjectUnionOf>
            		</owl:EquivalentClasses>

                    <owl:DisjointClasses>
                      <owl:Class IRI="B"/>
                      <owl:Class IRI="C"/>
                    </owl:DisjointClasses>

                </Tell>
        <IsKBSatisfiable kb="http://localhost/kb1"/>
        <IsClassSatisfiable kb="http://localhost/kb1">
        	<owl:Class IRI="A"/>
        </IsClassSatisfiable>
        <IsClassSatisfiable kb="http://localhost/kb1">
        	<owl:Class IRI="B"/>
        </IsClassSatisfiable>
        <IsClassSatisfiable kb="http://localhost/kb1">
        	<owl:Class IRI="C"/>
        </IsClassSatisfiable>
        <IsClassSatisfiable kb="http://localhost/kb1">
        	<owl:Class IRI="D"/>
        </IsClassSatisfiable>
        <GetSubClassHierarchy kb="http://localhost/kb1"/>
        <GetDisjointClasses kb="http://localhost/kb1">
        	<owl:Class IRI="A"/>
        </GetDisjointClasses>
        <GetDisjointClasses kb="http://localhost/kb1">
        	<owl:Class IRI="B"/>
        </GetDisjointClasses>
        <GetDisjointClasses kb="http://localhost/kb1">
        	<owl:Class IRI="C"/>
        </GetDisjointClasses>
        <GetDisjointClasses kb="http://localhost/kb1">
        	<owl:Class IRI="D"/>
        </GetDisjointClasses>
        <GetEquivalentClasses kb="http://localhost/kb1">
        	<owl:Class IRI="A"/>
        </GetEquivalentClasses>
        <GetEquivalentClasses kb="http://localhost/kb1">
        	<owl:Class IRI="B"/>
        </GetEquivalentClasses>
        <GetEquivalentClasses kb="http://localhost/kb1">
        	<owl:Class IRI="C"/>
        </GetEquivalentClasses>
        <GetEquivalentClasses kb="http://localhost/kb1">
        	<owl:Class IRI="D"/>
        </GetEquivalentClasses>
</RequestMessage>
XML;

        $owl2_input = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<Ontology xmlns="http://www.w3.org/2002/07/owl#"
          xml:base=""
          xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
          xmlns:xml="http://www.w3.org/XML/1998/namespace"
          xmlns:xsd="http://www.w3.org/2001/XMLSchema#"
          xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
          ontologyIRI="http://localhost/kb1">
                            <SubClassOf>
                              <Class IRI="#D"/>
                              <Class IRI="#A"/>
                            </SubClassOf>
                            <SubClassOf>
                              <Class IRI="#C"/>
                              <Class IRI="#A"/>
                            </SubClassOf>
                            <SubClassOf>
                              <Class IRI="#B"/>
                              <Class IRI="#A"/>
                            </SubClassOf>
                            <SubClassOf>
                              <Class IRI="#A"/>
                              <Class abbreviatedIRI="owl:Thing"/>
                            </SubClassOf>
                            <DisjointClasses>
                              <Class IRI="#D"/>
                              <Class IRI="#B"/>
                            </DisjointClasses>
                		        <EquivalentClasses>
                        		 <Class IRI="#A"/>
                        		  <ObjectUnionOf>
                            		<Class IRI="#B"/>
                            		<Class IRI="#C"/>
                        		 </ObjectUnionOf>
                    		    </EquivalentClasses>
                            <DisjointClasses>
                              <Class IRI="#B"/>
                              <Class IRI="#C"/>
                            </DisjointClasses>
</Ontology>
XML;


$answer_output = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<ResponseMessage xmlns="http://www.owllink.org/owllink#"
                 xmlns:owl="http://www.w3.org/2002/07/owl#">
  <KB kb="http://localhost/kb1"/>
  <OK/>
  <BooleanResponse result="true"/>
  <BooleanResponse result="true"/>
  <BooleanResponse result="true"/>
  <BooleanResponse result="true"/>
  <BooleanResponse result="true"/>
  <ClassHierarchy>
    <ClassSynset>
      <owl:Class abbreviatedIRI="owl:Nothing"/>
    </ClassSynset>
    <ClassSubClassesPair>
      <ClassSynset>
        <owl:Class abbreviatedIRI="owl:Thing"/>
      </ClassSynset>
      <SubClassSynsets>
        <ClassSynset>
          <owl:Class IRI="A"/>
        </ClassSynset>
      </SubClassSynsets>
    </ClassSubClassesPair>
    <ClassSubClassesPair>
      <ClassSynset>
        <owl:Class IRI="A"/>
      </ClassSynset>
      <SubClassSynsets>
        <ClassSynset>
          <owl:Class IRI="C"/>
        </ClassSynset>
        <ClassSynset>
          <owl:Class IRI="B"/>
        </ClassSynset>
      </SubClassSynsets>
    </ClassSubClassesPair>
    <ClassSubClassesPair>
      <ClassSynset>
        <owl:Class IRI="C"/>
      </ClassSynset>
      <SubClassSynsets>
        <ClassSynset>
          <owl:Class IRI="D"/>
        </ClassSynset>
      </SubClassSynsets>
    </ClassSubClassesPair>
  </ClassHierarchy>
  <ClassSynsets>
    <ClassSynset>
      <owl:Class abbreviatedIRI="owl:Nothing"/>
    </ClassSynset>
  </ClassSynsets>
  <ClassSynsets>
    <ClassSynset>
      <owl:Class abbreviatedIRI="owl:Nothing"/>
    </ClassSynset>
    <ClassSynset>
      <owl:Class IRI="C"/>
    </ClassSynset>
    <ClassSynset>
      <owl:Class IRI="D"/>
    </ClassSynset>
  </ClassSynsets>
  <ClassSynsets>
    <ClassSynset>
      <owl:Class abbreviatedIRI="owl:Nothing"/>
    </ClassSynset>
    <ClassSynset>
      <owl:Class IRI="B"/>
    </ClassSynset>
  </ClassSynsets>
  <ClassSynsets>
    <ClassSynset>
      <owl:Class abbreviatedIRI="owl:Nothing"/>
    </ClassSynset>
    <ClassSynset>
      <owl:Class IRI="B"/>
    </ClassSynset>
  </ClassSynsets>
  <SetOfClasses>
    <owl:Class IRI="A"/>
  </SetOfClasses>
  <SetOfClasses>
    <owl:Class IRI="B"/>
  </SetOfClasses>
  <SetOfClasses>
    <owl:Class IRI="C"/>
  </SetOfClasses>
  <SetOfClasses>
    <owl:Class IRI="D"/>
  </SetOfClasses>
</ResponseMessage>
XML;

$expected = <<<'EOT'
       {
           "satisfiable": {
               "kb" : true,
               "classes" : ["A","B","C","D"]
           },
           "unsatisfiable": {
              	"classes" : []
           },
           "graphical_suggestions" : {
              	"links" : []
           },
           "non_graphical_suggestion" : {
              	"links" : []
           },
           "reasoner" : {
              	"input" : "",
              	"output" : "",
                "owl2" : "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<Ontology xmlns=\"http:\/\/www.w3.org\/2002\/07\/owl#\" xml:base=\"\" xmlns:rdf=\"http:\/\/www.w3.org\/1999\/02\/22-rdf-syntax-ns#\" xmlns:xml=\"http:\/\/www.w3.org\/XML\/1998\/namespace\" xmlns:xsd=\"http:\/\/www.w3.org\/2001\/XMLSchema#\" xmlns:rdfs=\"http:\/\/www.w3.org\/2000\/01\/rdf-schema#\" ontologyIRI=\"http:\/\/localhost\/kb1\"><SubClassOf><Class IRI=\"#Class1\"\/><Class abbreviatedIRI=\"owl:Thing\"\/><\/SubClassOf><SubClassOf><Class IRI=\"#Student\"\/><Class abbreviatedIRI=\"owl:Thing\"\/><\/SubClassOf><SubClassOf><Class IRI=\"#Student_R_max\"\/><Class IRI=\"#Student\"\/><\/SubClassOf><SubClassOf><Class IRI=\"#Class2\"\/><Class IRI=\"#Student\"\/><\/SubClassOf><SubClassOf><Class IRI=\"#Class2_R_max\"\/><Class IRI=\"#Student_R_max\"\/><\/SubClassOf><SubClassOf><Class IRI=\"#Class2_R_max\"\/><Class IRI=\"#Class2\"\/><\/SubClassOf><SubClassOf><Class IRI=\"#Student_R_min\"\/><Class IRI=\"#Class2\"\/><\/SubClassOf><DisjointClasses><Class IRI=\"#Person\"\/><Class abbreviatedIRI=\"owl:Nothing\"\/><\/DisjointClasses><DisjointClasses><Class IRI=\"#Student\"\/><Class abbreviatedIRI=\"owl:Nothing\"\/><\/DisjointClasses><DisjointClasses><Class IRI=\"#Class1\"\/><Class abbreviatedIRI=\"owl:Nothing\"\/><\/DisjointClasses><DisjointClasses><Class IRI=\"#Class2\"\/><Class abbreviatedIRI=\"owl:Nothing\"\/><\/DisjointClasses><EquivalentClasses><Class IRI=\"#Person\"\/><Class IRI=\"#Class1\"\/><\/EquivalentClasses><EquivalentClasses><Class IRI=\"#Person\"\/><Class IRI=\"#Person\"\/><\/EquivalentClasses><EquivalentClasses><Class IRI=\"#Person\"\/><Class IRI=\"#Person_R_min\"\/><\/EquivalentClasses><EquivalentClasses><Class IRI=\"#Person\"\/><Class IRI=\"#Person_R_max\"\/><\/EquivalentClasses><EquivalentClasses><Class IRI=\"#Person\"\/><Class IRI=\"#Class1_R_max\"\/><\/EquivalentClasses><EquivalentClasses><Class IRI=\"#Person\"\/><Class IRI=\"#Class1_R_min\"\/><\/EquivalentClasses><EquivalentClasses><Class IRI=\"#Student\"\/><Class IRI=\"#Student\"\/><\/EquivalentClasses><EquivalentClasses><Class IRI=\"#Class1\"\/><Class IRI=\"#Class1\"\/><\/EquivalentClasses><EquivalentClasses><Class IRI=\"#Class1\"\/><Class IRI=\"#Person\"\/><\/EquivalentClasses><EquivalentClasses><Class IRI=\"#Class1\"\/><Class IRI=\"#Person_R_min\"\/><\/EquivalentClasses><EquivalentClasses><Class IRI=\"#Class1\"\/><Class IRI=\"#Person_R_max\"\/><\/EquivalentClasses><EquivalentClasses><Class IRI=\"#Class1\"\/><Class IRI=\"#Class1_R_max\"\/><\/EquivalentClasses><EquivalentClasses><Class IRI=\"#Class1\"\/><Class IRI=\"#Class1_R_min\"\/><\/EquivalentClasses><EquivalentClasses><Class IRI=\"#Class2\"\/><Class IRI=\"#Class2\"\/><\/EquivalentClasses><SubClassOf><Class IRI=\"#Person\"\/><Class abbreviatedIRI=\"owl:Thing\"\/><\/SubClassOf><SubClassOf><Class IRI=\"#Student\"\/><Class abbreviatedIRI=\"owl:Thing\"\/><\/SubClassOf><SubClassOf><ObjectSomeValuesFrom><ObjectProperty IRI=\"#R\"\/><Class abbreviatedIRI=\"owl:Thing\"\/><\/ObjectSomeValuesFrom><Class IRI=\"#Person\"\/><\/SubClassOf><SubClassOf><ObjectSomeValuesFrom><ObjectInverseOf><ObjectProperty IRI=\"#R\"\/><\/ObjectInverseOf><Class abbreviatedIRI=\"owl:Thing\"\/><\/ObjectSomeValuesFrom><Class IRI=\"#Student\"\/><\/SubClassOf><SubClassOf><Class IRI=\"#Person\"\/><ObjectMinCardinality cardinality=\"1\"><ObjectProperty IRI=\"#R\"\/><\/ObjectMinCardinality><\/SubClassOf><SubClassOf><Class IRI=\"#Person\"\/><ObjectMaxCardinality cardinality=\"1\"><ObjectProperty IRI=\"#R\"\/><\/ObjectMaxCardinality><\/SubClassOf><EquivalentClasses><Class IRI=\"#Person_R_min\"\/><ObjectIntersectionOf><Class IRI=\"#Person\"\/><ObjectMinCardinality cardinality=\"1\"><ObjectProperty IRI=\"#R\"\/><\/ObjectMinCardinality><\/ObjectIntersectionOf><\/EquivalentClasses><EquivalentClasses><Class IRI=\"#Person_R_max\"\/><ObjectIntersectionOf><Class IRI=\"#Person\"\/><ObjectMaxCardinality cardinality=\"1\"><ObjectProperty IRI=\"#R\"\/><\/ObjectMaxCardinality><\/ObjectIntersectionOf><\/EquivalentClasses><EquivalentClasses><Class IRI=\"#Student_R_min\"\/><ObjectIntersectionOf><Class IRI=\"#Student\"\/><ObjectMinCardinality cardinality=\"1\"><ObjectInverseOf><ObjectProperty IRI=\"#R\"\/><\/ObjectInverseOf><\/ObjectMinCardinality><\/ObjectIntersectionOf><\/EquivalentClasses><EquivalentClasses><Class IRI=\"#Student_R_max\"\/><ObjectIntersectionOf><Class IRI=\"#Student\"\/><ObjectMaxCardinality cardinality=\"1\"><ObjectInverseOf><ObjectProperty IRI=\"#R\"\/><\/ObjectInverseOf><\/ObjectMaxCardinality><\/ObjectIntersectionOf><\/EquivalentClasses><SubClassOf><Class IRI=\"#Class1\"\/><Class IRI=\"#Person\"\/><\/SubClassOf><SubClassOf><Class IRI=\"#Class2\"\/><Class IRI=\"#Student\"\/><\/SubClassOf><SubClassOf><ObjectSomeValuesFrom><ObjectProperty IRI=\"#R\"\/><Class abbreviatedIRI=\"owl:Thing\"\/><\/ObjectSomeValuesFrom><Class IRI=\"#Class1\"\/><\/SubClassOf><SubClassOf><ObjectSomeValuesFrom><ObjectInverseOf><ObjectProperty IRI=\"#R\"\/><\/ObjectInverseOf><Class abbreviatedIRI=\"owl:Thing\"\/><\/ObjectSomeValuesFrom><Class IRI=\"#Class2\"\/><\/SubClassOf><SubClassOf><Class IRI=\"#Class1\"\/><ObjectMinCardinality cardinality=\"1\"><ObjectProperty IRI=\"#R\"\/><\/ObjectMinCardinality><\/SubClassOf><EquivalentClasses><Class IRI=\"#Class1_R_min\"\/><ObjectIntersectionOf><Class IRI=\"#Class1\"\/><ObjectMinCardinality cardinality=\"1\"><ObjectProperty IRI=\"#R\"\/><\/ObjectMinCardinality><\/ObjectIntersectionOf><\/EquivalentClasses><EquivalentClasses><Class IRI=\"#Class1_R_max\"\/><ObjectIntersectionOf><Class IRI=\"#Class1\"\/><ObjectMaxCardinality cardinality=\"1\"><ObjectProperty IRI=\"#R\"\/><\/ObjectMaxCardinality><\/ObjectIntersectionOf><\/EquivalentClasses><EquivalentClasses><Class IRI=\"#Class2_R_min\"\/><ObjectIntersectionOf><Class IRI=\"#Class2\"\/><ObjectMinCardinality cardinality=\"1\"><ObjectInverseOf><ObjectProperty IRI=\"#R\"\/><\/ObjectInverseOf><\/ObjectMinCardinality><\/ObjectIntersectionOf><\/EquivalentClasses><EquivalentClasses><Class IRI=\"#Class2_R_max\"\/><ObjectIntersectionOf><Class IRI=\"#Class2\"\/><ObjectMaxCardinality cardinality=\"1\"><ObjectInverseOf><ObjectProperty IRI=\"#R\"\/><\/ObjectInverseOf><\/ObjectMaxCardinality><\/ObjectIntersectionOf><\/EquivalentClasses><\/Ontology>"
           }
       }
EOT;

        $oa = new CrowdAnalizer();
        $oa->generate_answer($query_input, $answer_output, $owl2_input);
        $oa->analize();
        $answer = $oa->get_answer();
        $owl2 = $answer->get_new_owl2()->to_string();
        var_dump($owl2);
        $answer->set_reasoner_input("");
        $answer->set_reasoner_output("");
        $actual = $answer->to_json();
//        var_dump($xml->to_string());
//        $out = $oa->get_newonto()->to_string();
//        var_dump($out);

//        $oa->analize();
//        $json_actual = $oa->get_answer();
//        $json_actual_sat_unsat = $json_actual->to_json();
//        $owllink_actual = $owllink->to_string();
/*        print("\n\n");
        print($actual);
        print("\n\n");
        print("\n\n");
        print($expected);
        print("\n\n"); */

//        $this->assertJsonStringEqualsJsonString($expected, $actual, true);
  //      $this->assertEquals($expected, $actual, true);
    }


}
