<?php
/*

   Copyright 2016 Gilia, Departamento de Teoría de la Computación, Universidad Nacional del Comahue

   Author: Gilia

   crowdUMLtest.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once("common.php");

// use function \load;
load("crowd_uml.php", "wicom/translator/strategies/");
load("owllinkbuilder.php", "wicom/translator/builders/");

use Wicom\Translator\Strategies\UMLcrowd;
use Wicom\Translator\Builders\OWLlinkBuilder;

/**
   # Warning!
   Don't use assertEqualXMLStructure()! It won't check for attributes values!

   It will only check for the amount of attributes.

   In order to keep interoperability with Protégé, roles on left sides of owl expressions
   must have fillers (top or class). Look at next role domain expression:

   <owl:SubClassOf>
     <owl:ObjectSomeValuesFrom>
         <owl:ObjectProperty IRI="r1"/>
         <owl:Class abbreviatedIRI="owl:Thing" />
     </owl:ObjectSomeValuesFrom>
     <owl:Class IRI="PhoneCall"/>
   </owl:SubClassOf>

   However, right sides accept roles without fillers. Look at following max cardinality expression:

   <owl:SubClassOf>
          <owl:Class IRI="PhoneCall"/>
         <owl:ObjectMaxCardinality cardinality="1">
              <owl:ObjectProperty IRI="r1"/>
         </owl:ObjectMaxCardinality>
   </owl:SubClassOf>

 */


class UMLcrowdTest extends PHPUnit\Framework\TestCase
{


    ##
    # Test if translate works properly with binary roles many-to-many
    public function testTranslateBinaryRolesWithoutClass0N(){
		$json = <<< EOT
{
"classes": [{"name":"PhoneCall", "attrs":[], "methods":[]},
			 {"name":"Phone", "attrs":[], "methods":[]}],
"links": [{"name": "r1", "classes":["PhoneCall","Phone"], "multiplicity":[null,null], "type":"association"}]
}
EOT;

        $expected = <<< EOT
<?xml version="1.0" encoding="UTF-8"?>
<RequestMessage xmlns="http://www.owllink.org/owllink#"
		xmlns:owl="http://www.w3.org/2002/07/owl#"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="http://www.owllink.org/owllink# http://www.owllink.org/owllink-20091116.xsd"
    xml:base="">
  <CreateKB kb="http://localhost/kb1" />
  <Tell kb="http://localhost/kb1">
    <owl:SubClassOf>
      <owl:Class IRI="PhoneCall" />
      <owl:Class abbreviatedIRI="owl:Thing" />
    </owl:SubClassOf>
    <owl:SubClassOf>
      <owl:Class IRI="Phone" />
      <owl:Class abbreviatedIRI="owl:Thing" />
    </owl:SubClassOf>

    <owl:SubClassOf>
  		<owl:ObjectSomeValuesFrom>
          <owl:ObjectProperty IRI="r1"/>
          <owl:Class abbreviatedIRI="owl:Thing" />
      </owl:ObjectSomeValuesFrom>
  		<owl:Class IRI="PhoneCall"/>
    </owl:SubClassOf>

    <owl:SubClassOf>
  		<owl:ObjectSomeValuesFrom>
  			<owl:ObjectInverseOf>
              	<owl:ObjectProperty IRI="r1"/>
  			</owl:ObjectInverseOf>
        <owl:Class abbreviatedIRI="owl:Thing" />
      </owl:ObjectSomeValuesFrom>
  		<owl:Class IRI="Phone"/>
    </owl:SubClassOf>

    <owl:EquivalentClasses>
        <owl:Class IRI="PhoneCall_r1_min"/>
        <owl:ObjectIntersectionOf>
            <owl:Class IRI="PhoneCall"/>
            <owl:ObjectMinCardinality cardinality="1">
                <owl:ObjectProperty IRI="r1"/>
            </owl:ObjectMinCardinality>
        </owl:ObjectIntersectionOf>
    </owl:EquivalentClasses>
    <owl:EquivalentClasses>
        <owl:Class IRI="PhoneCall_r1_max"/>
        <owl:ObjectIntersectionOf>
            <owl:Class IRI="PhoneCall"/>
            <owl:ObjectMaxCardinality cardinality="1">
                <owl:ObjectProperty IRI="r1"/>
            </owl:ObjectMaxCardinality>
        </owl:ObjectIntersectionOf>
    </owl:EquivalentClasses>
    <owl:EquivalentClasses>
        <owl:Class IRI="Phone_r1_min"/>
        <owl:ObjectIntersectionOf>
            <owl:Class IRI="Phone"/>
            <owl:ObjectMinCardinality cardinality="1">
                <owl:ObjectInverseOf>
                    <owl:ObjectProperty IRI="r1"/>
                </owl:ObjectInverseOf>
            </owl:ObjectMinCardinality>
        </owl:ObjectIntersectionOf>
    </owl:EquivalentClasses>
    <owl:EquivalentClasses>
        <owl:Class IRI="Phone_r1_max"/>
        <owl:ObjectIntersectionOf>
            <owl:Class IRI="Phone"/>
            <owl:ObjectMaxCardinality cardinality="1">
                <owl:ObjectInverseOf>
                    <owl:ObjectProperty IRI="r1"/>
                </owl:ObjectInverseOf>
            </owl:ObjectMaxCardinality>
        </owl:ObjectIntersectionOf>
    </owl:EquivalentClasses>
  </Tell>
</RequestMessage>
EOT;

        $strategy = new UMLcrowd();
        $builder = new OWLlinkBuilder();

        $builder->insert_header();
        $strategy->translate($json, $builder);
        $builder->insert_footer();

        $actual = $builder->get_product();
        $actual = $actual->to_string();
        $this->assertXmlStringEqualsXmlString($expected, $actual,true);
  }


    ##
    # Test if translate works properly with binary roles many-to-many

    public function testTranslateBinaryRolesWithoutClass01(){
		$json = <<< EOT
{
"classes": [{"name":"PhoneCall", "attrs":[], "methods":[]},
			 {"name":"Phone", "attrs":[], "methods":[]}],
"links": [{"name": "r1", "classes":["PhoneCall","Phone"], "multiplicity":["0..1","0..1"], "type":"association"}]
}
EOT;

        $expected = <<< EOT
<?xml version="1.0" encoding="UTF-8"?>
<RequestMessage xmlns="http://www.owllink.org/owllink#"
		xmlns:owl="http://www.w3.org/2002/07/owl#"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="http://www.owllink.org/owllink# http://www.owllink.org/owllink-20091116.xsd"
    xml:base="">
  <CreateKB kb="http://localhost/kb1" />
  <Tell kb="http://localhost/kb1">
    <owl:SubClassOf>
      <owl:Class IRI="PhoneCall" />
      <owl:Class abbreviatedIRI="owl:Thing" />
    </owl:SubClassOf>
    <owl:SubClassOf>
      <owl:Class IRI="Phone" />
      <owl:Class abbreviatedIRI="owl:Thing" />
    </owl:SubClassOf>

    <owl:SubClassOf>
  		<owl:ObjectSomeValuesFrom>
          <owl:ObjectProperty IRI="r1"/>
          <owl:Class abbreviatedIRI="owl:Thing" />
      </owl:ObjectSomeValuesFrom>
  		<owl:Class IRI="PhoneCall"/>
    </owl:SubClassOf>

    <owl:SubClassOf>
  		<owl:ObjectSomeValuesFrom>
  			<owl:ObjectInverseOf>
              	<owl:ObjectProperty IRI="r1"/>
  			</owl:ObjectInverseOf>
        <owl:Class abbreviatedIRI="owl:Thing" />
      </owl:ObjectSomeValuesFrom>
  		<owl:Class IRI="Phone"/>
    </owl:SubClassOf>

    <owl:SubClassOf>
           <owl:Class IRI="PhoneCall"/>
           <owl:ObjectMaxCardinality cardinality="1">
               <owl:ObjectProperty IRI="r1"/>
          </owl:ObjectMaxCardinality>
    </owl:SubClassOf>

    <owl:EquivalentClasses>
        <owl:Class IRI="PhoneCall_r1_min"/>
        <owl:ObjectIntersectionOf>
            <owl:Class IRI="PhoneCall"/>
            <owl:ObjectMinCardinality cardinality="1">
                <owl:ObjectProperty IRI="r1"/>
            </owl:ObjectMinCardinality>
        </owl:ObjectIntersectionOf>
    </owl:EquivalentClasses>
    <owl:EquivalentClasses>
        <owl:Class IRI="PhoneCall_r1_max"/>
        <owl:ObjectIntersectionOf>
            <owl:Class IRI="PhoneCall"/>
            <owl:ObjectMaxCardinality cardinality="1">
                <owl:ObjectProperty IRI="r1"/>
            </owl:ObjectMaxCardinality>
        </owl:ObjectIntersectionOf>
    </owl:EquivalentClasses>

    <owl:SubClassOf>
           <owl:Class IRI="Phone"/>
           <owl:ObjectMaxCardinality cardinality="1">
             <owl:ObjectInverseOf>
                 <owl:ObjectProperty IRI="r1"/>
             </owl:ObjectInverseOf>
          </owl:ObjectMaxCardinality>
    </owl:SubClassOf>


    <owl:EquivalentClasses>
        <owl:Class IRI="Phone_r1_min"/>
        <owl:ObjectIntersectionOf>
            <owl:Class IRI="Phone"/>
            <owl:ObjectMinCardinality cardinality="1">
                <owl:ObjectInverseOf>
                    <owl:ObjectProperty IRI="r1"/>
                </owl:ObjectInverseOf>
            </owl:ObjectMinCardinality>
        </owl:ObjectIntersectionOf>
    </owl:EquivalentClasses>
    <owl:EquivalentClasses>
        <owl:Class IRI="Phone_r1_max"/>
        <owl:ObjectIntersectionOf>
            <owl:Class IRI="Phone"/>
            <owl:ObjectMaxCardinality cardinality="1">
                <owl:ObjectInverseOf>
                    <owl:ObjectProperty IRI="r1"/>
                </owl:ObjectInverseOf>
            </owl:ObjectMaxCardinality>
        </owl:ObjectIntersectionOf>
    </owl:EquivalentClasses>
  </Tell>
</RequestMessage>
EOT;

        $strategy = new UMLcrowd();
        $builder = new OWLlinkBuilder();

        $builder->insert_header();
        $strategy->translate($json, $builder);
        $builder->insert_footer();

        $actual = $builder->get_product();
        $actual = $actual->to_string();
        $this->assertXmlStringEqualsXmlString($expected, $actual,true);
    }



    ##
    # Test if translate works properly with binary roles many-to-many
    public function testTranslateBinaryRolesWithoutClass1N(){
		$json = <<< EOT
{
"classes": [{"name":"PhoneCall", "attrs":[], "methods":[]},
			 {"name":"Phone", "attrs":[], "methods":[]}],
"links": [{"name": "r1", "classes":["PhoneCall","Phone"], "multiplicity":["1..*","1..*"], "type":"association"}]
}
EOT;

        //TODO: Complete XML!
        $expected = <<< EOT
<?xml version="1.0" encoding="UTF-8"?>
<RequestMessage xmlns="http://www.owllink.org/owllink#"
		xmlns:owl="http://www.w3.org/2002/07/owl#"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="http://www.owllink.org/owllink# http://www.owllink.org/owllink-20091116.xsd"
    xml:base="">
  <CreateKB kb="http://localhost/kb1" />
  <Tell kb="http://localhost/kb1">
    <owl:SubClassOf>
      <owl:Class IRI="PhoneCall" />
      <owl:Class abbreviatedIRI="owl:Thing" />
    </owl:SubClassOf>
    <owl:SubClassOf>
      <owl:Class IRI="Phone" />
      <owl:Class abbreviatedIRI="owl:Thing" />
    </owl:SubClassOf>

    <owl:SubClassOf>
  		<owl:ObjectSomeValuesFrom>
          <owl:ObjectProperty IRI="r1"/>
          <owl:Class abbreviatedIRI="owl:Thing" />
      </owl:ObjectSomeValuesFrom>
  		<owl:Class IRI="PhoneCall"/>
    </owl:SubClassOf>

    <owl:SubClassOf>
  		<owl:ObjectSomeValuesFrom>
  			<owl:ObjectInverseOf>
              	<owl:ObjectProperty IRI="r1"/>
  			</owl:ObjectInverseOf>
        <owl:Class abbreviatedIRI="owl:Thing" />
      </owl:ObjectSomeValuesFrom>
  		<owl:Class IRI="Phone"/>
    </owl:SubClassOf>

    <owl:SubClassOf>
       	  <owl:Class IRI="PhoneCall"/>
          <owl:ObjectMinCardinality cardinality="1">
               <owl:ObjectProperty IRI="r1"/>
          </owl:ObjectMinCardinality>
    </owl:SubClassOf>

    <owl:EquivalentClasses>
        <owl:Class IRI="PhoneCall_r1_min"/>
        <owl:ObjectIntersectionOf>
            <owl:Class IRI="PhoneCall"/>
            <owl:ObjectMinCardinality cardinality="1">
                <owl:ObjectProperty IRI="r1"/>
            </owl:ObjectMinCardinality>
        </owl:ObjectIntersectionOf>
    </owl:EquivalentClasses>
    <owl:EquivalentClasses>
        <owl:Class IRI="PhoneCall_r1_max"/>
        <owl:ObjectIntersectionOf>
            <owl:Class IRI="PhoneCall"/>
            <owl:ObjectMaxCardinality cardinality="1">
                <owl:ObjectProperty IRI="r1"/>
            </owl:ObjectMaxCardinality>
        </owl:ObjectIntersectionOf>
    </owl:EquivalentClasses>

    <owl:SubClassOf>
       	  <owl:Class IRI="Phone"/>
          <owl:ObjectMinCardinality cardinality="1">
			       <owl:ObjectInverseOf>
               	<owl:ObjectProperty IRI="r1"/>
             </owl:ObjectInverseOf>
          </owl:ObjectMinCardinality>
    </owl:SubClassOf>


    <owl:EquivalentClasses>
        <owl:Class IRI="Phone_r1_min"/>
        <owl:ObjectIntersectionOf>
            <owl:Class IRI="Phone"/>
            <owl:ObjectMinCardinality cardinality="1">
                <owl:ObjectInverseOf>
                    <owl:ObjectProperty IRI="r1"/>
                </owl:ObjectInverseOf>
            </owl:ObjectMinCardinality>
        </owl:ObjectIntersectionOf>
    </owl:EquivalentClasses>
    <owl:EquivalentClasses>
        <owl:Class IRI="Phone_r1_max"/>
        <owl:ObjectIntersectionOf>
            <owl:Class IRI="Phone"/>
            <owl:ObjectMaxCardinality cardinality="1">
                <owl:ObjectInverseOf>
                    <owl:ObjectProperty IRI="r1"/>
                </owl:ObjectInverseOf>
            </owl:ObjectMaxCardinality>
        </owl:ObjectIntersectionOf>
    </owl:EquivalentClasses>


  </Tell>
</RequestMessage>
EOT;

        $strategy = new UMLcrowd();
        $builder = new OWLlinkBuilder();

        $builder->insert_header(); // Without this, loading the DOMDocument
        // will throw error for the owl namespace
        $strategy->translate($json, $builder);
        $builder->insert_footer();

        $actual = $builder->get_product();
        $actual = $actual->to_string();
        $this->assertXmlStringEqualsXmlString($expected, $actual,true);
    }


    ##
    # Test if translate works properly with binary roles many-to-many
    public function testTranslateBinaryRolesWithoutClass11(){
		$json = <<< EOT
{
"classes": [{"name":"PhoneCall", "attrs":[], "methods":[]},
			 {"name":"Phone", "attrs":[], "methods":[]}],
"links": [{"name": "r1", "classes":["PhoneCall","Phone"], "multiplicity":["1..1","1..1"], "type":"association"}]
}
EOT;

        $expected = <<< EOT
<?xml version="1.0" encoding="UTF-8"?>
<RequestMessage xmlns="http://www.owllink.org/owllink#"
		xmlns:owl="http://www.w3.org/2002/07/owl#"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="http://www.owllink.org/owllink# http://www.owllink.org/owllink-20091116.xsd"
    xml:base="">
  <CreateKB kb="http://localhost/kb1" />
  <Tell kb="http://localhost/kb1">
    <owl:SubClassOf>
      <owl:Class IRI="PhoneCall" />
      <owl:Class abbreviatedIRI="owl:Thing" />
    </owl:SubClassOf>
    <owl:SubClassOf>
      <owl:Class IRI="Phone" />
      <owl:Class abbreviatedIRI="owl:Thing" />
    </owl:SubClassOf>

    <owl:SubClassOf>
  		<owl:ObjectSomeValuesFrom>
          <owl:ObjectProperty IRI="r1"/>
          <owl:Class abbreviatedIRI="owl:Thing" />
      </owl:ObjectSomeValuesFrom>
  		<owl:Class IRI="PhoneCall"/>
    </owl:SubClassOf>

    <owl:SubClassOf>
  		<owl:ObjectSomeValuesFrom>
  			<owl:ObjectInverseOf>
              	<owl:ObjectProperty IRI="r1"/>
  			</owl:ObjectInverseOf>
        <owl:Class abbreviatedIRI="owl:Thing" />
      </owl:ObjectSomeValuesFrom>
  		<owl:Class IRI="Phone"/>
    </owl:SubClassOf>

    <owl:SubClassOf>
       	  <owl:Class IRI="PhoneCall"/>
          <owl:ObjectMinCardinality cardinality="1">
               <owl:ObjectProperty IRI="r1"/>
           </owl:ObjectMinCardinality>
    </owl:SubClassOf>
    <owl:SubClassOf>
       	  <owl:Class IRI="PhoneCall"/>
          <owl:ObjectMaxCardinality cardinality="1">
               <owl:ObjectProperty IRI="r1"/>
           </owl:ObjectMaxCardinality>
    </owl:SubClassOf>

    <owl:EquivalentClasses>
        <owl:Class IRI="PhoneCall_r1_min"/>
        <owl:ObjectIntersectionOf>
            <owl:Class IRI="PhoneCall"/>
            <owl:ObjectMinCardinality cardinality="1">
                <owl:ObjectProperty IRI="r1"/>
            </owl:ObjectMinCardinality>
        </owl:ObjectIntersectionOf>
    </owl:EquivalentClasses>
    <owl:EquivalentClasses>
        <owl:Class IRI="PhoneCall_r1_max"/>
        <owl:ObjectIntersectionOf>
            <owl:Class IRI="PhoneCall"/>
            <owl:ObjectMaxCardinality cardinality="1">
                <owl:ObjectProperty IRI="r1"/>
            </owl:ObjectMaxCardinality>
        </owl:ObjectIntersectionOf>
    </owl:EquivalentClasses>

    <owl:SubClassOf>
       	  <owl:Class IRI="Phone"/>
          <owl:ObjectMinCardinality cardinality="1">
			        <owl:ObjectInverseOf>
               	<owl:ObjectProperty IRI="r1"/>
             </owl:ObjectInverseOf>
           </owl:ObjectMinCardinality>
    </owl:SubClassOf>
    <owl:SubClassOf>
       	  <owl:Class IRI="Phone"/>
          <owl:ObjectMaxCardinality cardinality="1">
			       <owl:ObjectInverseOf>
               	<owl:ObjectProperty IRI="r1"/>
             </owl:ObjectInverseOf>
          </owl:ObjectMaxCardinality>
    </owl:SubClassOf>


    <owl:EquivalentClasses>
        <owl:Class IRI="Phone_r1_min"/>
        <owl:ObjectIntersectionOf>
            <owl:Class IRI="Phone"/>
            <owl:ObjectMinCardinality cardinality="1">
                <owl:ObjectInverseOf>
                    <owl:ObjectProperty IRI="r1"/>
                </owl:ObjectInverseOf>
            </owl:ObjectMinCardinality>
        </owl:ObjectIntersectionOf>
    </owl:EquivalentClasses>
    <owl:EquivalentClasses>
        <owl:Class IRI="Phone_r1_max"/>
        <owl:ObjectIntersectionOf>
            <owl:Class IRI="Phone"/>
            <owl:ObjectMaxCardinality cardinality="1">
                <owl:ObjectInverseOf>
                    <owl:ObjectProperty IRI="r1"/>
                </owl:ObjectInverseOf>
            </owl:ObjectMaxCardinality>
        </owl:ObjectIntersectionOf>
    </owl:EquivalentClasses>

  </Tell>
</RequestMessage>
EOT;

        $strategy = new UMLcrowd();
        $builder = new OWLlinkBuilder();

        $builder->insert_header(); // Without this, loading the DOMDocument
        // will throw error for the owl namespace
        $strategy->translate($json, $builder);
        $builder->insert_footer();

        $actual = $builder->get_product();
        $actual = $actual->to_string();
        $this->assertXmlStringEqualsXmlString($expected, $actual,true);
    }



    ##
    # Test if translate works properly with binary roles many-to-many > 1
    public function testTranslateBinaryRolesWithoutClassMN(){
		$json = <<< EOT
{
"classes": [{"name":"PhoneCall", "attrs":[], "methods":[]},
			 {"name":"Phone", "attrs":[], "methods":[]}],
"links": [{"name": "r1", "classes":["PhoneCall","Phone"], "multiplicity":["1..4","2..9"], "type":"association"}]
}
EOT;

        //TODO: Complete XML!
        $expected = <<< EOT
<?xml version="1.0" encoding="UTF-8"?>
<RequestMessage xmlns="http://www.owllink.org/owllink#"
		xmlns:owl="http://www.w3.org/2002/07/owl#"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="http://www.owllink.org/owllink# http://www.owllink.org/owllink-20091116.xsd"
    xml:base="">
  <CreateKB kb="http://localhost/kb1" />
  <Tell kb="http://localhost/kb1">
    <owl:SubClassOf>
      <owl:Class IRI="PhoneCall" />
      <owl:Class abbreviatedIRI="owl:Thing" />
    </owl:SubClassOf>
    <owl:SubClassOf>
      <owl:Class IRI="Phone" />
      <owl:Class abbreviatedIRI="owl:Thing" />
    </owl:SubClassOf>

    <owl:SubClassOf>
  		<owl:ObjectSomeValuesFrom>
          <owl:ObjectProperty IRI="r1"/>
          <owl:Class abbreviatedIRI="owl:Thing" />
      </owl:ObjectSomeValuesFrom>
  		<owl:Class IRI="PhoneCall"/>
    </owl:SubClassOf>

    <owl:SubClassOf>
  		<owl:ObjectSomeValuesFrom>
  			<owl:ObjectInverseOf>
              	<owl:ObjectProperty IRI="r1"/>
  			</owl:ObjectInverseOf>
        <owl:Class abbreviatedIRI="owl:Thing" />
      </owl:ObjectSomeValuesFrom>
  		<owl:Class IRI="Phone"/>
    </owl:SubClassOf>


    <owl:SubClassOf>
       	  <owl:Class IRI="PhoneCall"/>
          <owl:ObjectMinCardinality cardinality="2">
               <owl:ObjectProperty IRI="r1"/>
          </owl:ObjectMinCardinality>
    </owl:SubClassOf>

    <owl:SubClassOf>
       	  <owl:Class IRI="PhoneCall"/>
          <owl:ObjectMaxCardinality cardinality="9">
               <owl:ObjectProperty IRI="r1"/>
          </owl:ObjectMaxCardinality>
    </owl:SubClassOf>

    <owl:EquivalentClasses>
        <owl:Class IRI="PhoneCall_r1_min"/>
        <owl:ObjectIntersectionOf>
            <owl:Class IRI="PhoneCall"/>
            <owl:ObjectMinCardinality cardinality="2">
                <owl:ObjectProperty IRI="r1"/>
            </owl:ObjectMinCardinality>
        </owl:ObjectIntersectionOf>
    </owl:EquivalentClasses>
    <owl:EquivalentClasses>
        <owl:Class IRI="PhoneCall_r1_max"/>
        <owl:ObjectIntersectionOf>
            <owl:Class IRI="PhoneCall"/>
            <owl:ObjectMaxCardinality cardinality="9">
                <owl:ObjectProperty IRI="r1"/>
            </owl:ObjectMaxCardinality>
        </owl:ObjectIntersectionOf>
    </owl:EquivalentClasses>


    <owl:SubClassOf>
       	  <owl:Class IRI="Phone"/>
          <owl:ObjectMinCardinality cardinality="1">
			       <owl:ObjectInverseOf>
               	<owl:ObjectProperty IRI="r1"/>
             </owl:ObjectInverseOf>
           </owl:ObjectMinCardinality>
    </owl:SubClassOf>
    <owl:SubClassOf>
       	  <owl:Class IRI="Phone"/>
          <owl:ObjectMaxCardinality cardinality="4">
			       <owl:ObjectInverseOf>
               	<owl:ObjectProperty IRI="r1"/>
             </owl:ObjectInverseOf>
           </owl:ObjectMaxCardinality>
    </owl:SubClassOf>


    <owl:EquivalentClasses>
        <owl:Class IRI="Phone_r1_min"/>
        <owl:ObjectIntersectionOf>
            <owl:Class IRI="Phone"/>
            <owl:ObjectMinCardinality cardinality="1">
                <owl:ObjectInverseOf>
                    <owl:ObjectProperty IRI="r1"/>
                </owl:ObjectInverseOf>
            </owl:ObjectMinCardinality>
        </owl:ObjectIntersectionOf>
    </owl:EquivalentClasses>
    <owl:EquivalentClasses>
        <owl:Class IRI="Phone_r1_max"/>
        <owl:ObjectIntersectionOf>
            <owl:Class IRI="Phone"/>
            <owl:ObjectMaxCardinality cardinality="4">
                <owl:ObjectInverseOf>
                    <owl:ObjectProperty IRI="r1"/>
                </owl:ObjectInverseOf>
            </owl:ObjectMaxCardinality>
        </owl:ObjectIntersectionOf>
    </owl:EquivalentClasses>

  </Tell>
</RequestMessage>
EOT;

        $strategy = new UMLcrowd();
        $builder = new OWLlinkBuilder();

        $builder->insert_header(); // Without this, loading the DOMDocument
        // will throw error for the owl namespace
        $strategy->translate($json, $builder);
        $builder->insert_footer();

        $actual = $builder->get_product();
        $actual = $actual->to_string();
        $this->assertXmlStringEqualsXmlString($expected, $actual,true);
    }



    # Test generalization is translated properly.
    public function testTranslateGeneralization(){
        //TODO: Complete JSON!
        $json = <<< EOT

{"classes": [
    {"attrs":[], "methods":[], "name": "PhoneCall"},
    {"attrs":[], "methods":[], "name": "MobileCall"}],
 "links": [
     {"classes": ["MobileCall"],
      "multiplicity": null,
      "name": "r1",
      "type": "generalization",
      "parent": "PhoneCall",
      "constraint": []}
	]
}
EOT;
        $expected = <<< EOT
<?xml version="1.0" encoding="UTF-8"?>
<RequestMessage xmlns="http://www.owllink.org/owllink#"
		xmlns:owl="http://www.w3.org/2002/07/owl#"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="http://www.owllink.org/owllink# http://www.owllink.org/owllink-20091116.xsd"
    xml:base="">
  <CreateKB kb="http://localhost/kb1" />
  <Tell kb="http://localhost/kb1">

    <owl:SubClassOf>
      <owl:Class IRI="PhoneCall" />
      <owl:Class abbreviatedIRI="owl:Thing" />
    </owl:SubClassOf>

    <!-- Generalization -->

    <owl:SubClassOf>
      <owl:Class IRI="MobileCall" />
	    <owl:Class IRI="PhoneCall" />
    </owl:SubClassOf>

  </Tell>
</RequestMessage>
EOT;

        $strategy = new UMLcrowd();
        $builder = new OWLlinkBuilder();

        $builder->insert_header();
        $strategy->translate($json, $builder);
        $builder->insert_footer();

        $actual = $builder->get_product();
        $actual = $actual->to_string();

        $this->assertXmlStringEqualsXmlString($expected, $actual, TRUE);
    }


    # Test composed generalization (without constraints!).
    public function testTranslateComposedGen(){
        //TODO: Complete JSON!
        $json = <<<'EOT'
{"classes": [
    {"attrs":[], "methods":[], "name": "Person"},
    {"attrs":[], "methods":[], "name": "Employee"},
    {"attrs":[], "methods":[], "name": "Employer"},
    {"attrs":[], "methods":[], "name": "Director"}],
 "links": [
     {"classes": ["Employee", "Employer", "Director"],
      "multiplicity": null,
      "name": "r1",
      "type": "generalization",
      "parent": "Person",
      "constraint": []}
	]
}
EOT;
        $expected = <<<'EOT'
<?xml version="1.0" encoding="UTF-8"?>
<RequestMessage xmlns="http://www.owllink.org/owllink#"
		xmlns:owl="http://www.w3.org/2002/07/owl#"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="http://www.owllink.org/owllink# http://www.owllink.org/owllink-20091116.xsd"
    xml:base="">
  <CreateKB kb="http://localhost/kb1" />
  <Tell kb="http://localhost/kb1">

    <owl:SubClassOf>
      <owl:Class IRI="Person" />
      <owl:Class abbreviatedIRI="owl:Thing" />
    </owl:SubClassOf>

    <!-- Generalization -->

    <owl:SubClassOf>
      <owl:Class IRI="Employee" />
	     <owl:Class IRI="Person" />
    </owl:SubClassOf>
    <owl:SubClassOf>
      <owl:Class IRI="Employer" />
	     <owl:Class IRI="Person" />
    </owl:SubClassOf>
    <owl:SubClassOf>
      <owl:Class IRI="Director" />
	     <owl:Class IRI="Person" />
    </owl:SubClassOf>

    <owl:SubClassOf>
      <owl:ObjectUnionOf>
        <owl:Class IRI="Employee" />
        <owl:Class IRI="Employer" />
        <owl:Class IRI="Director" />
      </owl:ObjectUnionOf>
      <owl:Class IRI="Person" />
    </owl:SubClassOf>
  </Tell>
  <!-- <ReleaseKB kb="http://localhost/kb1" /> -->
</RequestMessage>
EOT;

        $strategy = new UMLcrowd();
        $builder = new OWLlinkBuilder();

        $builder->insert_header();
        $strategy->translate($json, $builder);
        $builder->insert_footer();

        $actual = $builder->get_product();
        $actual = $actual->to_string();

        $this->assertXmlStringEqualsXmlString($expected, $actual, TRUE);
    }


    # Test generalization with disjoint constraint is translated properly.
    public function testTranslateGenDisjoint(){
        //TODO: Complete JSON!
        $json = <<<'EOT'
{"classes": [
    {"attrs":[], "methods":[], "name": "Person"},
    {"attrs":[], "methods":[], "name": "Employee"},
    {"attrs":[], "methods":[], "name": "Employer"},
    {"attrs":[], "methods":[], "name": "Director"}],
 "links": [
     {"classes": ["Employee", "Employer", "Director"],
      "multiplicity": null,
      "name": "r1",
      "type": "generalization",
      "parent": "Person",
      "constraint": ["disjoint"]}
	]
}
EOT;
        $expected = <<<'EOT'
<?xml version="1.0" encoding="UTF-8"?>
<RequestMessage xmlns="http://www.owllink.org/owllink#"
		xmlns:owl="http://www.w3.org/2002/07/owl#"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="http://www.owllink.org/owllink# http://www.owllink.org/owllink-20091116.xsd"
    xml:base="">
  <CreateKB kb="http://localhost/kb1" />
  <Tell kb="http://localhost/kb1">

    <owl:SubClassOf>
      <owl:Class IRI="Person" />
      <owl:Class abbreviatedIRI="owl:Thing" />
    </owl:SubClassOf>

    <!-- Generalization -->

    <owl:SubClassOf>
      <owl:Class IRI="Employee" />
	     <owl:Class IRI="Person" />
    </owl:SubClassOf>
    <owl:SubClassOf>
      <owl:Class IRI="Employer" />
	     <owl:Class IRI="Person" />
    </owl:SubClassOf>
    <owl:SubClassOf>
      <owl:Class IRI="Director" />
	     <owl:Class IRI="Person" />
    </owl:SubClassOf>

    <owl:SubClassOf>
      <owl:ObjectUnionOf>
        <owl:Class IRI="Employee" />
        <owl:Class IRI="Employer" />
        <owl:Class IRI="Director" />
      </owl:ObjectUnionOf>
      <owl:Class IRI="Person" />
    </owl:SubClassOf>

    <owl:DisjointClasses>
      <owl:Class IRI="Employee" />
      <owl:Class IRI="Employer" />
      <owl:Class IRI="Director" />
    </owl:DisjointClasses>

  </Tell>
  <!-- <ReleaseKB kb="http://localhost/kb1" /> -->
</RequestMessage>
EOT;

        $strategy = new UMLcrowd();
        $builder = new OWLlinkBuilder();

        $builder->insert_header(); // Without this, loading the DOMDocument
        // will throw error for the owl namespace
        $strategy->translate($json, $builder);
        $builder->insert_footer();

        $actual = $builder->get_product();
        $actual = $actual->to_string();

        $this->assertXmlStringEqualsXmlString($expected, $actual, TRUE);
    }


    # Test generalization with covering constraint is translated properly.
    public function testTranslateGenCovering(){
        //TODO: Complete JSON!
        $json = <<<'EOT'
{"classes": [
    {"attrs":[], "methods":[], "name": "Person"},
    {"attrs":[], "methods":[], "name": "Employee"},
    {"attrs":[], "methods":[], "name": "Employer"},
    {"attrs":[], "methods":[], "name": "Director"}],
 "links": [
     {"classes": ["Employee", "Employer", "Director"],
      "multiplicity": null,
      "name": "r1",
      "type": "generalization",
      "parent": "Person",
      "constraint": ["covering"]}
	]
}
EOT;
        $expected = <<<'EOT'
<?xml version="1.0" encoding="UTF-8"?>
<RequestMessage xmlns="http://www.owllink.org/owllink#"
		xmlns:owl="http://www.w3.org/2002/07/owl#"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="http://www.owllink.org/owllink# http://www.owllink.org/owllink-20091116.xsd"
    xml:base="">
  <CreateKB kb="http://localhost/kb1" />
  <Tell kb="http://localhost/kb1">

    <owl:SubClassOf>
      <owl:Class IRI="Person" />
      <owl:Class abbreviatedIRI="owl:Thing" />
    </owl:SubClassOf>

    <!-- Generalization -->

    <owl:SubClassOf>
      <owl:Class IRI="Employee" />
	  <owl:Class IRI="Person" />
    </owl:SubClassOf>
    <owl:SubClassOf>
      <owl:Class IRI="Employer" />
	  <owl:Class IRI="Person" />
    </owl:SubClassOf>
    <owl:SubClassOf>
      <owl:Class IRI="Director" />
	  <owl:Class IRI="Person" />
    </owl:SubClassOf>

    <owl:SubClassOf>
      <owl:ObjectUnionOf>
          <owl:Class IRI="Employee" />
          <owl:Class IRI="Employer" />
          <owl:Class IRI="Director" />
      </owl:ObjectUnionOf>
      <owl:Class IRI="Person" />
    </owl:SubClassOf>

    <owl:SubClassOf>
      <owl:Class IRI="Person" />
      <owl:ObjectUnionOf>
          <owl:Class IRI="Employee" />
          <owl:Class IRI="Employer" />
          <owl:Class IRI="Director" />
      </owl:ObjectUnionOf>
    </owl:SubClassOf>

  </Tell>
  <!-- <ReleaseKB kb="http://localhost/kb1" /> -->
</RequestMessage>
EOT;

        $strategy = new UMLcrowd();
        $builder = new OWLlinkBuilder();

        $builder->insert_header(); // Without this, loading the DOMDocument
        // will throw error for the owl namespace
        $strategy->translate($json, $builder);
        $builder->insert_footer();

        $actual = $builder->get_product();
        $actual = $actual->to_string();

        $this->assertXmlStringEqualsXmlString($expected, $actual, TRUE);
    }


    /**
       Test for checking UMLcrowd::translate_queries method for full reasoning.
     */
    public function test_translate_queries_full_reasoning(){
        //TODO: Complete JSON!
        $json = <<<EOT

{"classes": [
    {"attrs":[], "methods":[], "name": "PhoneCall"},
    {"attrs":[], "methods":[], "name": "MobileCall"}],
 "links": [
     {"classes": ["MobileCall"],
      "multiplicity": null,
      "name": "r1",
      "type": "generalization",
      "parent": "PhoneCall",
      "constraint": []}
	]
}
EOT;
        $expected = <<< EOT
<?xml version="1.0" encoding="UTF-8"?>
<RequestMessage xmlns="http://www.owllink.org/owllink#"
		xmlns:owl="http://www.w3.org/2002/07/owl#"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="http://www.owllink.org/owllink# http://www.owllink.org/owllink-20091116.xsd"
    xml:base="">
  <CreateKB kb="http://localhost/kb1" />
  <Tell kb="http://localhost/kb1"/>
  <IsKBSatisfiable kb="http://localhost/kb1"/>
  <IsClassSatisfiable kb="http://localhost/kb1"><owl:Class IRI="PhoneCall"/></IsClassSatisfiable>
  <IsClassSatisfiable kb="http://localhost/kb1"><owl:Class IRI="MobileCall"/></IsClassSatisfiable>
  <GetSubClassHierarchy kb="http://localhost/kb1"/>
  <GetDisjointClasses kb="http://localhost/kb1">
    <owl:Class IRI="PhoneCall"/>
  </GetDisjointClasses>
  <GetDisjointClasses kb="http://localhost/kb1">
    <owl:Class IRI="MobileCall"/>
  </GetDisjointClasses>
  <GetEquivalentClasses kb="http://localhost/kb1">
    <owl:Class IRI="PhoneCall"/>
  </GetEquivalentClasses>
  <GetEquivalentClasses kb="http://localhost/kb1">
    <owl:Class IRI="MobileCall"/>
  </GetEquivalentClasses>
</RequestMessage>
EOT;

        $strategy = new UMLcrowd();
        $builder = new OWLlinkBuilder();

        $builder->insert_header(); // Without this, loading the DOMDocument
        // will throw error for the owl namespace
        $strategy->translate_queries($json, $builder);
        $builder->insert_footer();

        $actual = $builder->get_product();
        $actual = $actual->to_string();

        $this->assertXmlStringEqualsXmlString($expected, $actual, TRUE);
    }





    /**
       Test for checking Strategy::translate_queries method only for class satisfiability.

       @todo Checks for adding more tests for generating queries methods.
     */
    public function test_translate_queries_infer(){

    }
}
