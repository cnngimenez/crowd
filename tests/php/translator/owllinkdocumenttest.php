<?php
/*

   Copyright 2016 Giménez, Christian

   Author: Giménez, Christian

   owllinkdocumenttest.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once("common.php");

//use function \load;
load("owllinkdocument.php","wicom/translator/documents/");


use Wicom\Translator\Documents\OWLlinkDocument;

class OWLlinkDocumentTest extends PHPUnit\Framework\TestCase{

    public function testConstructor(){
        $expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<RequestMessage xmlns=\"http://www.owllink.org/owllink#\"
		xmlns:owl=\"http://www.w3.org/2002/07/owl#\"
		xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
		xsi:schemaLocation=\"http://www.owllink.org/owllink# http://www.owllink.org/owllink-20091116.xsd\"
    xml:base=\"\">
</RequestMessage>";

        $d = new OWLlinkDocument();
        $d->start_document();
        $d->end_document();
        $actual = $d->to_string();

        $expected = process_xmlspaces($expected);
        $actual = process_xmlspaces($actual);
        $this->assertEqualXMLStructure($expected, $actual, true);
    }

    public function testInsertCreateKB(){
        $expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<RequestMessage xmlns=\"http://www.owllink.org/owllink#\"
		xmlns:owl=\"http://www.w3.org/2002/07/owl#\"
		xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
		xsi:schemaLocation=\"http://www.owllink.org/owllink# http://www.owllink.org/owllink-20091116.xsd\"
    xml:base=\"\">
  <CreateKB kb=\"http://localhost/kb1\" />
</RequestMessage>";

        $d = new OWLlinkDocument();
        $d->start_document();
        $d->insert_create_kb("http://localhost/kb1");
        $d->end_document();

        $actual = $d->to_string();

        $expected = process_xmlspaces($expected);
        $actual = process_xmlspaces($actual);
        $this->assertEqualXMLStructure($expected, $actual, true);
    }

    public function testClasses(){
        $expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<RequestMessage xmlns=\"http://www.owllink.org/owllink#\"
		xmlns:owl=\"http://www.w3.org/2002/07/owl#\"
		xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
		xsi:schemaLocation=\"http://www.owllink.org/owllink# http://www.owllink.org/owllink-20091116.xsd\"
    xml:base=\"\">
    <CreateKB kb=\"http://localhost/kb1\" />
    <Tell kb=\"http://localhost/kb1\">
        <owl:Class IRI=\"Hi World\" />
    </Tell>
    <ReleaseKB kb=\"http://localhost/kb1\" />
</RequestMessage>";

        $d = new OWLlinkDocument();
        $d->start_document();
        $d->insert_create_kb("http://localhost/kb1");
        $d->start_tell();
        $d->insert_class("Hi World");
        $d->end_tell();
        $d->insert_release_kb("http://localhost/kb1");
        $d->end_document();

        $actual = $d->to_string();

        $expected = process_xmlspaces($expected);
        $actual = process_xmlspaces($actual);
        $this->assertEqualXMLStructure($expected, $actual, true);
    }

    public function testSubclass(){
        $expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<RequestMessage xmlns=\"http://www.owllink.org/owllink#\"
		xmlns:owl=\"http://www.w3.org/2002/07/owl#\"
		xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
		xsi:schemaLocation=\"http://www.owllink.org/owllink# http://www.owllink.org/owllink-20091116.xsd\"
    xml:base=\"\">
    <CreateKB kb=\"http://localhost/kb1\" />
    <Tell kb=\"http://localhost/kb1\">
       <owl:SubClassOf>
       <owl:Class IRI=\"Hi World\" />
       <owl:Class abbreviatedIRI=\"owl:Thing\" />
       </owl:SubClassOf>
    </Tell>
    <ReleaseKB kb=\"http://localhost/kb1\" />
</RequestMessage>";

        $d = new OWLlinkDocument();
        $d->start_document();
        $d->insert_create_kb("http://localhost/kb1");
        $d->start_tell();
        $d->insert_subclassof("Hi World", "owl:Thing", false, true);
        $d->end_tell();
        $d->insert_release_kb("http://localhost/kb1");
        $d->end_document();

        $actual = $d->to_string();

        $expected = process_xmlspaces($expected);
        $actual = process_xmlspaces($actual);
        $this->assertEqualXMLStructure($expected, $actual, true);
    }

    /* Tests for checking OWLlink ClassHierarchy Responses  */

    /*  <xsd:element name="ClassHierarchy">
          <xsd:complexType>
            <xsd:complexContent>
              <xsd:extension base="ol:KBResponse">
                <xsd:sequence><!-- the synset of unsatisfiable classes -->
                  <xsd:element name="ClassSynset" type="ol:ClassSynset"/>
                  <xsd:element name="ClassSubClassesPair" minOccurs="0" maxOccurs="unbounded">
                    <xsd:complexType>
                      <xsd:sequence>
                        <xsd:element name="ClassSynset" type="ol:ClassSynset"/>
                        <xsd:element ref="ol:SubClassSynsets"/>
                      </xsd:sequence>
                    </xsd:complexType>
                  </xsd:element>
                </xsd:sequence>
              </xsd:extension>
            </xsd:complexContent>
          </xsd:complexType>
        </xsd:element>  */

    public function testClassHierarchyResponse(){
        $expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<ResponseMessage xmlns=\"http://www.owllink.org/owllink#\"
                 xmlns:owl=\"http://www.w3.org/2002/07/owl#\">
    <OK/>
    <ClassHierarchy>
      <ClassSynset>
        <owl:Class abbreviatedIRI=\"owl:Nothing\"/>
      </ClassSynset>
      <ClassSubClassesPair>
        <ClassSynset>
          <owl:Class abbreviatedIRI=\"owl:Thing\"/>
        </ClassSynset>
        <SubClassSynsets>
          <ClassSynset>
            <owl:Class IRI=\"Person\"/>
          </ClassSynset>
        </SubClassSynsets>
      </ClassSubClassesPair>
      <ClassSubClassesPair>
        <ClassSynset>
          <owl:Class IRI=\"Person\"/>
        </ClassSynset>
        <SubClassSynsets>
          <ClassSynset>
            <owl:Class IRI=\"Director\"/>
          </ClassSynset>
          <ClassSynset>
            <owl:Class IRI=\"Employer\"/>
          </ClassSynset>
          <ClassSynset>
            <owl:Class IRI=\"Employee\"/>
          </ClassSynset>
        </SubClassSynsets>
      </ClassSubClassesPair>
    </ClassHierarchy>
  </ResponseMessage>";

        $d = new OWLlinkDocumentResponse();
        $d->start_document_response();
        $d->insert_kb_response("http://localhost/kb1");
        $d->insert_kb_response("OK");
        $d->start_class_hierarchy();
        $d->insert_class_synset_bottom();
        $d->insert_classSubClassesPair("owl:Thing","Person");
        $d->insert_classSubClassesPair("Person",["Director", "Employer", "Employee"]);
        $d->end_class_hierarchy();
        $d->end_document_response();

        $actual = $d->to_string();

        $expected = process_xmlspaces($expected);
        $actual = process_xmlspaces($actual);
        $this->assertEqualXMLStructure($expected, $actual, true);
    }
}

?>
