<?php
/*

   Copyright 2018 GILIA

   Author: GILIA

   decodertest.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once("common.php");

//use function \load;
load("crowd_uml.php", "wicom/translator/strategies/");
load("berardistrat.php", "wicom/translator/strategies/");
load("owllinkbuilder.php", "wicom/translator/builders/");
load("translator.php", "wicom/translator/");

use Wicom\Translator\Strategies\Berardi;
use Wicom\Translator\Strategies\UMLcrowd;
use Wicom\Translator\Builders\OWLlinkBuilder;
use Wicom\Translator\Translator;

class DecoderTest extends PHPUnit\Framework\TestCase
{

    public function test_to_json_simple(){

        $expected = '{"classes": [{"name": "Class5", "attrs":[], "methods":[]}], "links" : []}';
        $owl2 = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<Ontology xmlns="http://www.w3.org/2002/07/owl#"
          xml:base=""
          xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
          xmlns:xml="http://www.w3.org/XML/1998/namespace"
          xmlns:xsd="http://www.w3.org/2001/XMLSchema#"
          xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
          ontologyIRI="http://localhost/kb1">
          <SubClassOf>
            <Class IRI="#Class5"/>
            <Class abbreviatedIRI="owl:Thing"/>
          </SubClassOf>
</Ontology>
XML;

        $strategy = new UMLcrowd();
        $jsonbuilder = new UMLJSONBuilder();
        $decoder = new Decoder($strategy, $jsonbuilder);

        $actual = $decoder->to_umljson($owl2);

        $this->assertJsonStringEqualsJsonString($expected, $actual, true);
    }

    public function test_to_owlink_with_thing_and_sub(){

        $json = '{"classes": [{"name":"MobilePhone", "attrs":[], "methods":[]},
        			 {"name":"Phone", "attrs":[], "methods":[]}],
        "links": [{"classes":["MobilePhone"], "name": "r1", "multiplicity":null,
                   "type":"generalization", "parent" : "Phone", "constraint" : []}]}';

        $expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<RequestMessage xmlns=\"http://www.owllink.org/owllink#\"
		xmlns:owl=\"http://www.w3.org/2002/07/owl#\"
		xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
		xsi:schemaLocation=\"http://www.owllink.org/owllink# http://www.owllink.org/owllink-20091116.xsd\"
    xml:base=\"\">
<CreateKB kb=\"http://localhost/kb1\" />
<Tell kb=\"http://localhost/kb1\">
  <owl:SubClassOf>
    <owl:Class IRI=\"Phone\" />
    <owl:Class abbreviatedIRI=\"owl:Thing\" />
  </owl:SubClassOf>
  <owl:SubClassOf>
    <owl:Class IRI=\"MobilePhone\" />
    <owl:Class IRI=\"Phone\" />
  </owl:SubClassOf>
</Tell>
<IsKBSatisfiable kb=\"http://localhost/kb1\" />
<IsClassSatisfiable kb=\"http://localhost/kb1\">
  <owl:Class IRI=\"Phone\" />
</IsClassSatisfiable>
<IsClassSatisfiable kb=\"http://localhost/kb1\">
  <owl:Class IRI=\"MobilePhone\" />
</IsClassSatisfiable>
<GetSubClassHierarchy kb=\"http://localhost/kb1\" />
<GetSubObjectPropertyHierarchy kb=\"http://localhost/kb1\" />
</RequestMessage>";

        $strategy = new UMLcrowd();
        $builder = new OWLlinkBuilder();
        $translator = new Translator($strategy, $builder);

        $actual = $translator->to_owllink($json);

        $expected = process_xmlspaces($expected);
        $actual = process_xmlspaces($actual);
        $this->assertEqualXMLStructure($expected, $actual, true);
    }


    public function test_to_owlink_with_thing_and_sub_no_compose(){

        $json = '{"classes": [{"name":"MobilePhone", "attrs":[], "methods":[]},
        			 {"name":"Phone", "attrs":[], "methods":[]},
               {"name":"FixedPhone", "attrs":[], "methods":[]}],
        "links": [{"classes":["MobilePhone"], "name": "r1", "multiplicity":null,
                   "type":"generalization", "parent" : "Phone", "constraint" : []},
                   {"classes":["FixedPhone"], "name": "r2", "multiplicity":null,
                              "type":"generalization", "parent" : "Phone", "constraint" : []}]}';

        $expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<RequestMessage xmlns=\"http://www.owllink.org/owllink#\"
		xmlns:owl=\"http://www.w3.org/2002/07/owl#\"
		xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
		xsi:schemaLocation=\"http://www.owllink.org/owllink# http://www.owllink.org/owllink-20091116.xsd\"
    xml:base=\"\">
<CreateKB kb=\"http://localhost/kb1\" />
<Tell kb=\"http://localhost/kb1\">
  <owl:SubClassOf>
    <owl:Class IRI=\"Phone\" />
    <owl:Class abbreviatedIRI=\"owl:Thing\" />
  </owl:SubClassOf>
  <owl:SubClassOf>
    <owl:Class IRI=\"MobilePhone\" />
    <owl:Class IRI=\"Phone\" />
  </owl:SubClassOf>
  <owl:SubClassOf>
    <owl:Class IRI=\"FixedPhone\" />
    <owl:Class IRI=\"Phone\" />
  </owl:SubClassOf>
</Tell>
<IsKBSatisfiable kb=\"http://localhost/kb1\" />
<IsClassSatisfiable kb=\"http://localhost/kb1\">
  <owl:Class IRI=\"Phone\" />
</IsClassSatisfiable>
<IsClassSatisfiable kb=\"http://localhost/kb1\">
  <owl:Class IRI=\"MobilePhone\" />
</IsClassSatisfiable>
<IsClassSatisfiable kb=\"http://localhost/kb1\">
  <owl:Class IRI=\"FixedPhone\" />
</IsClassSatisfiable>
<GetSubClassHierarchy kb=\"http://localhost/kb1\" />
<GetSubObjectPropertyHierarchy kb=\"http://localhost/kb1\" />
</RequestMessage>";

        $strategy = new UMLcrowd();
        $builder = new OWLlinkBuilder();
        $translator = new Translator($strategy, $builder);

        $actual = $translator->to_owllink($json);

        $expected = process_xmlspaces($expected);
        $actual = process_xmlspaces($actual);
        $this->assertEqualXMLStructure($expected, $actual, true);
    }

    public function test_to_owlink_with_thing_and_sub_compose(){

        $json = '{"classes": [{"name":"MobilePhone", "attrs":[], "methods":[]},
        			 {"name":"Phone", "attrs":[], "methods":[]},
               {"name":"FixedPhone", "attrs":[], "methods":[]}],
        "links": [{"classes":["MobilePhone","FixedPhone"], "name": "r1", "multiplicity":null,
                   "type":"generalization", "parent" : "Phone", "constraint" : ["covering"]}]}';

        $expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<RequestMessage xmlns=\"http://www.owllink.org/owllink#\"
		xmlns:owl=\"http://www.w3.org/2002/07/owl#\"
		xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
		xsi:schemaLocation=\"http://www.owllink.org/owllink# http://www.owllink.org/owllink-20091116.xsd\"
    xml:base=\"\">
<CreateKB kb=\"http://localhost/kb1\" />
<Tell kb=\"http://localhost/kb1\">
  <owl:SubClassOf>
    <owl:Class IRI=\"Phone\" />
    <owl:Class abbreviatedIRI=\"owl:Thing\" />
  </owl:SubClassOf>
  <owl:SubClassOf>
    <owl:Class IRI=\"MobilePhone\" />
    <owl:Class IRI=\"Phone\" />
  </owl:SubClassOf>
  <owl:SubClassOf>
    <owl:Class IRI=\"FixedPhone\" />
    <owl:Class IRI=\"Phone\" />
  </owl:SubClassOf>
  <owl:SubClassOf>
    <owl:Class IRI=\"Phone\" />
    <owl:ObjectUnionOf>
        <owl:Class IRI=\"MobilePhone\" />
        <owl:Class IRI=\"FixedPhone\" />
    </owl:ObjectUnionOf>
  </owl:SubClassOf>
</Tell>
<IsKBSatisfiable kb=\"http://localhost/kb1\" />
<IsClassSatisfiable kb=\"http://localhost/kb1\">
  <owl:Class IRI=\"Phone\" />
</IsClassSatisfiable>
<IsClassSatisfiable kb=\"http://localhost/kb1\">
  <owl:Class IRI=\"MobilePhone\" />
</IsClassSatisfiable>
<IsClassSatisfiable kb=\"http://localhost/kb1\">
  <owl:Class IRI=\"FixedPhone\" />
</IsClassSatisfiable>
<GetSubClassHierarchy kb=\"http://localhost/kb1\" />
<GetSubObjectPropertyHierarchy kb=\"http://localhost/kb1\" />
</RequestMessage>";

        $strategy = new UMLcrowd();
        $builder = new OWLlinkBuilder();
        $translator = new Translator($strategy, $builder);

        $actual = $translator->to_owllink($json);

        var_dump($actual);

        $expected = process_xmlspaces($expected);
        $actual = process_xmlspaces($actual);
        $this->assertEqualXMLStructure($expected, $actual, true);
    }


    public function test_JSON_to_OWLlink_with_users_owllink(){
        //TODO: Complete JSON!
        $json = '{"classes": [{"attrs":[], "methods":[], "name": "Hi World"}], "links" : [], "owllink": "<hiworld></hiworld>"}';
        //TODO: Complete XML!
        $expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<RequestMessage xmlns=\"http://www.owllink.org/owllink#\"
		xmlns:owl=\"http://www.w3.org/2002/07/owl#\"
		xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
		xsi:schemaLocation=\"http://www.owllink.org/owllink# http://www.owllink.org/owllink-20091116.xsd\"
    xml:base=\"\">
<CreateKB kb=\"http://localhost/kb1\" />
<Tell kb=\"http://localhost/kb1\">
  <owl:SubClassOf>
    <owl:Class IRI=\"Hi World\" />
    <owl:Class abbreviatedIRI=\"owl:Thing\" />
  </owl:SubClassOf>
</Tell>
<hiworld></hiworld>
<IsKBSatisfiable kb=\"http://localhost/kb1\" />
<IsClassSatisfiable kb=\"http://localhost/kb1\">
  <owl:Class IRI=\"Hi World\" />
</IsClassSatisfiable>
<GetSubClassHierarchy kb=\"http://localhost/kb1\" />
<GetSubObjectPropertyHierarchy kb=\"http://localhost/kb1\" />
</RequestMessage>";

        $strategy = new UMLcrowd();
        $builder = new OWLlinkBuilder();
        $translator = new Translator($strategy, $builder);

        $actual = $translator->to_owllink($json);

        $expected = process_xmlspaces($expected);
        $actual = process_xmlspaces($actual);
        $this->assertEqualXMLStructure($expected, $actual, true);
    }
}

?>
