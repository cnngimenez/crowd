# test_mymodel.coffee --
# Copyright (C) 2017 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


QUnit.test("model.MyModel.get_joint", (assert) ->
    assert.ok(false, "Returns new joint because wasn't created");
    assert.ok(false, "Returns null because no factory provided");
    assert.ok(false, "Returns already created because it was created");
)

QUnit.test("model.MyModel.has_classid", (assert) ->
    assert.ok(false, "Returns false because joint doesn't exists")
    assert.ok(false, "Returns true")
    assert.ok(false, "Returns false")
)
