#! /usr/bin/fish


# Copyright 2018 Giménez, Christian

# Author: Giménez, Christian   

# download-reasoners.fish

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set baseurl "http://crowd.fi.uncoma.edu.ar/reasoners"
set reasoners_urls Konclude Racer sparqldl.jar

function download-all --description="Download all reasoners."	
	for reasoner in $reasoners_urls
		if test -f $reasoner
			set_color brown
			echo $reasoner already exists.
			set_color
		else
			set_color --bold
			echo Downloading $reasoner
			set_color
			wget -c "$baseurl/$reasoner"
			chmod +x $reasoner
		end
		
	end
end

if test -d run
	cd run
	download-all
else
	set_color red
	echo "run directory was not founded"
	set_color
	echo "Are you in the main crowd directory?
	Execute the script like this:"
	echo
	set_color green
	echo "    scripts/download-reasoners.fish"
	echo
	set_color
end
cd ..
