;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((coffee-mode
  (standard-indent . 2))
 (php-mode
  (fill-column . 80)))

